from django.conf.urls import url

from tesserae.apps.reports_app.views import reports_home, download_all_csv, download_daily, download_hourly, \
    download_issues, get_all_overrides, get_all_participant_daily, get_all_issues

urlpatterns = [
    url(r'^home', reports_home, name='reports_home'),
    url(r'^download_all/$', download_all_csv, name='download_all_csv'),
    url(r'^download_daily/$', get_all_participant_daily, name='download_daily'),
    url(r'^download_hourly/$', download_hourly, name='download_hourly'),
    url(r'^download_issues/$', download_issues, name='download_issues'),
    url(r'^download_overrides/$', get_all_overrides, name='download_overrides'),
    url(r'^download_issues_json/$', get_all_issues, name='get_all_issues'),
]
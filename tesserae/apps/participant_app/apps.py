from django.apps import AppConfig


class ParticipantsConfig(AppConfig):
    name = 'tesserae.apps.participant_app'

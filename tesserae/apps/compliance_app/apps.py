from django.apps import AppConfig


class ComplianceAppConfig(AppConfig):
    name = 'tesserae.apps.compliance_app'

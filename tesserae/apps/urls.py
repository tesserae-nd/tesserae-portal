from django.conf.urls import include, url

from tesserae.apps.base_portal.views.dashboard import StaffDashboardView, ParticipantDashboardView, \
    avg_stream_compliance_chart_ajax, overall_compliance_chart_ajax, stream_compliance_chart_ajax, \
    participant_filter_issues_ajax, staff_filter_issues_ajax, staff_filter__unassigned_issues_ajax, \
    get_average_compliance_data, get_overall_compliance, get_stream_compliance

urlpatterns = [
    url(r'^staff/', include('tesserae.apps.staff_app.urls', namespace='staff')),

    # Ajax calls for changing charts on staff dashboard
    url(r'^staff-dashboard/avg-stream-compliance-chart', avg_stream_compliance_chart_ajax,
        name='avg-stream-compliance-chart'),
    url(r'^staff-dashboard/overall-compliance-chart', overall_compliance_chart_ajax,
        name='overall-compliance-chart'),
    url(r'^staff-dashboard/stream-compliance-chart', stream_compliance_chart_ajax,
        name='stream-compliance-chart'),

    # Ajax call for filtering issues on dashboards
    url(r'^dashboard/filter-issues', participant_filter_issues_ajax,
        name='participant_filter_issues'),
    url(r'^staff-dashboard/filter-issues', staff_filter_issues_ajax,
        name='staff_filter_issues'),
    url(r'^staff-dashboard/filter-unassigned-issues', staff_filter__unassigned_issues_ajax,
        name='staff_filter__unassigned_issues'),

    url(r'^issues/', include('tesserae.apps.issue_app.urls', namespace='issues')),
    url(r'^staff-dashboard', StaffDashboardView.as_view(), name='staff_dashboard'),
    url(r'^dashboard', ParticipantDashboardView.as_view(), name='participant_dashboard'),
    url(r'^compliance/', include('tesserae.apps.compliance_app.urls', namespace='compliance')),
    url(r'^payments/', include('tesserae.apps.payment_app.urls', namespace='payments')),
    url(r'^participant/', include('tesserae.apps.participant_app.urls', namespace='participant')),
    url(r'^reports/', include('tesserae.apps.reports_app.urls', namespace='reports')),

    # get average compliance
    url(r'^get-average-compliance', get_average_compliance_data, name='get_average_compliance_data'),

    # get overall compliance
    url(r'^get-overall-compliance', get_overall_compliance, name='get_overall_compliance'),

    # get stream compliance
    url(r'^get-stream-compliance', get_stream_compliance, name='get_stream_compliance'),

]

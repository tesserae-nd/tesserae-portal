from allauth.socialaccount.models import SocialAccount
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.http import HttpResponseRedirect
from django.urls import reverse_lazy
from django.views.generic import CreateView, TemplateView
from django import forms

# Create your views here.
from tesserae.apps.participant_app.forms import NewParticipantForm
from tesserae.apps.participant_app.models import Participant
from tesserae.apps.users.models import User


class ParticipantCreate(LoginRequiredMixin, UserPassesTestMixin, CreateView):
    """
    Create View for new Participants
    """
    model = Participant
    template_name = 'participant/create_participant.html'
    form_class = NewParticipantForm
    success_url = reverse_lazy('portal:participant:success_participant_create')

    def __init__(self):
        super(ParticipantCreate, self).__init__()
        self.object = None

    def form_valid(self, form):
        print(self.request.POST)
        self.object = form.save()

        # Create new user object
        new_user_object = User(email=self.request.POST['gmail'], username=self.request.POST['gmail'].split('@')[0])
        new_user_object.save()

        # Create new social account object
        # new_social_acct = SocialAccount(user=new_user_object, provider='Google', uid=)

        return HttpResponseRedirect(self.get_success_url())

    def test_func(self):
        if self.request.user.is_staff:
            return True
        else:
            return False


class ParticipantCreateSuccess(LoginRequiredMixin, UserPassesTestMixin, TemplateView):
    template_name = 'participant/success_create_participant.html'

    def get_context_data(self, **kwargs):
        context = super(ParticipantCreateSuccess, self).get_context_data(**kwargs)

        return context

    def test_func(self):
        return self.request.user.is_staff

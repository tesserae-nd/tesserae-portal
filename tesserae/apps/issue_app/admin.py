from django.contrib import admin

# Register your models here.
from tesserae.apps.issue_app.models import IssueCategory, IssueStatus, Issue, IssueNote, IssueContactMethod

admin.site.register(IssueCategory)
admin.site.register(IssueStatus)
admin.site.register(IssueContactMethod)


@admin.register(Issue)
class IssueAdmin(admin.ModelAdmin):
    list_display = ('subject', 'description', 'reported_by_participant', 'date_reported', 'status', 'category',
                    'assigned_to', 'last_updated')

admin.site.register(IssueNote)

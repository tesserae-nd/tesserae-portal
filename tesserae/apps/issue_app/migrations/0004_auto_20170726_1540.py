# -*- coding: utf-8 -*-
# Generated by Django 1.10.7 on 2017-07-26 15:40
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('issue_app', '0003_auto_20170726_1540'),
    ]

    operations = [
        migrations.AlterField(
            model_name='issue',
            name='priority',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='issue_priority', to='issue_app.IssuePriority'),
        ),
    ]

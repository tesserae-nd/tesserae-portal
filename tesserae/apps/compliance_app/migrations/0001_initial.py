# -*- coding: utf-8 -*-
# Generated by Django 1.10.7 on 2017-09-11 18:49
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='ComplianceType',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('type_name', models.CharField(max_length=20)),
            ],
        ),
        migrations.CreateModel(
            name='ParticipantCompliance',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('compliance_date', models.DateField()),
                ('compliance_value', models.PositiveIntegerField()),
                ('compliance_type', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='compliance_type', to='compliance_app.ComplianceType')),
                ('participant', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='participant_user', to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]

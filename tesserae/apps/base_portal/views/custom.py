from allauth.account.views import LogoutView
from allauth.socialaccount.models import SocialAccount
from allauth.socialaccount.signals import pre_social_login
from django.core.exceptions import PermissionDenied, ObjectDoesNotExist
from django.dispatch import receiver
from django.urls import reverse
from django.views.generic import RedirectView

from tesserae.apps.participant_app.models import Participant
from tesserae.apps.staff_app.models import Staff
from tesserae.apps.users.models import User, UserLogin


class ProfileRedirectView(RedirectView):
    """
    Redirect from 'login' where it takes you to '/accounts/profile/'.
    It redirects to 'root' page or 'next' if any is filled out.
    """
    def get_redirect_url(self, *args, **kwargs):

        print(self.request.user.email)
        url = "#"
        if Participant.objects.filter(gmail=self.request.user.email).exists():
            url = "portal:participant_dashboard"
        if Staff.objects.filter(gmail=self.request.user.email).exists():
            user_obj = User.objects.get(email=self.request.user.email)
            user_obj.is_staff = True
            user_obj.save()
            print("Marked user as staff...")
            url = "portal:staff_dashboard"

        print("===========================")
        print("IN THE PROFILE REDIRECT VIEW")
        print(self.request.user)
        print(url)
        print("===========================")

        # Log the user's login in the database
        user_login = UserLogin.objects.create(user=self.request.user)

        return self.request.GET.get('next', reverse(url))


class CustomLogoutView(LogoutView):
    """
    Custom view that allows changing of the template provided by 'allauth' to use 'our' templates.
    NOTE: View does not add or remove any functionality!
    """
    template_name = "allauth/logout.html"


class CustomEmailLoginView(RedirectView):

    def get_redirect_url(self, *args, **kwargs):
        return "/accounts/google/login/?action=reauthenticate"


@receiver(pre_social_login)
def validate_email_during_login(sender, request, sociallogin, **kwargs):
    """
    This function catches the signal for social login and checks if the email address
    that the user is signing in with is in the participant or staff lists
    """

    # Get the email address of the person attempting to log in
    email_attempting_login = sociallogin.user.email

    if email_attempting_login:
        if Participant.objects.filter(gmail=email_attempting_login).exists():
            print("===========================")
            print("Participant with email " + email_attempting_login + " found!")
            print("===========================")
        elif Staff.objects.filter(gmail=email_attempting_login).exists():
            print("===========================")
            print("Staff with email " + email_attempting_login + " found!")
            print("===========================")
        else:
            print("===========================")
            print("Invalid Email Login Attempt!  " + email_attempting_login)
            print("===========================")
            # Delete social account, social application token, and user
            user_obj = User.objects.filter(email=email_attempting_login)
            SocialAccount.objects.filter(user=user_obj).delete()
            user_obj.delete()
            raise PermissionDenied

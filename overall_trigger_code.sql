﻿CREATE OR REPLACE FUNCTION compute_overall() RETURNS trigger AS $$
	DECLARE
		sum int default 0;
		count int default 1;
		avg decimal default 0.0;
        BEGIN
                IF (SELECT type_name FROM compliance_app_compliancetype WHERE id = NEW.compliance_type_id) != 'Overall'
                THEN
			raise notice '%', 'UPDATE!';
			sum = (SELECT SUM(compliance_value) FROM compliance_app_participantcompliance WHERE participant_id = NEW.participant_id and compliance_date = NEW.compliance_date and compliance_type_id != (select id from compliance_app_compliancetype where type_name = 'Overall'));
			count = (SELECT COUNT(compliance_value) FROM compliance_app_participantcompliance WHERE participant_id = NEW.participant_id and  compliance_date = NEW.compliance_date and compliance_type_id != (select id from compliance_app_compliancetype where type_name = 'Overall'));

			raise notice '%', sum;
			raise notice '%', count;
			avg = sum::decimal / count::decimal;
			-- now remove any record that may exist for "Overall"
			DELETE FROM compliance_app_participantcompliance WHERE participant_id = NEW.participant_id and compliance_date = NEW.compliance_date and compliance_type_id = (SELECT id FROM compliance_app_compliancetype WHERE type_name = 'Overall');
			INSERT INTO compliance_app_participantcompliance (compliance_date, compliance_value, compliance_type_id, participant_id, calculated_on) values (NEW.compliance_date, avg::int, (SELECT id FROM compliance_app_compliancetype WHERE type_name = 'Overall'), NEW.participant_id, now());
		ELSE
			raise notice '%', 'DO NOT UPDATE';
                END IF;

                RETURN NEW;
        END;
$$ LANGUAGE plpgsql;


CREATE OR REPLACE FUNCTION compute_overall_delete() RETURNS trigger AS $$
	DECLARE
		sum int default 0;
		count int default 1;
		avg decimal default 0.0;
        BEGIN
                IF (SELECT type_name FROM compliance_app_compliancetype WHERE id = OLD.compliance_type_id) != 'Overall'
                THEN
			raise notice '%', 'UPDATE!';
			sum = (SELECT SUM(compliance_value) FROM compliance_app_participantcompliance WHERE participant_id = OLD.participant_id and compliance_date = OLD.compliance_date and compliance_type_id != (select id from compliance_app_compliancetype where type_name = 'Overall'));
			count = (SELECT COUNT(compliance_value) FROM compliance_app_participantcompliance WHERE participant_id = OLD.participant_id and  compliance_date = OLD.compliance_date and compliance_type_id != (select id from compliance_app_compliancetype where type_name = 'Overall'));

			raise notice '%', sum;
			raise notice '%', count;
			avg = sum::decimal / count::decimal;
			-- now remove any record that may exist for "Overall"
			IF avg != NULL
			THEN
				DELETE FROM compliance_app_participantcompliance WHERE participant_id = OLD.participant_id and compliance_date = OLD.compliance_date and compliance_type_id = (SELECT id FROM compliance_app_compliancetype WHERE type_name = 'Overall');
				INSERT INTO compliance_app_participantcompliance (compliance_date, compliance_value, compliance_type_id, participant_id, calculated_on) values (OLD.compliance_date, avg::int, (SELECT id FROM compliance_app_compliancetype WHERE type_name = 'Overall'), OLD.participant_id, now());
			END IF;
		ELSE
			raise notice '%', 'DO NOT UPDATE';
                END IF;

                RETURN OLD;
        END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER compute_overall_trigger AFTER UPDATE OR INSERT ON compliance_app_participantcompliance
FOR EACH ROW EXECUTE PROCEDURE compute_overall();

CREATE TRIGGER compute_overall_delete_trigger AFTER DELETE ON compliance_app_participantcompliance
FOR EACH ROW EXECUTE PROCEDURE compute_overall_delete();
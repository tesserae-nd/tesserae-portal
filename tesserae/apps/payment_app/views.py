import csv
import datetime
from dateutil import parser
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.core.files.storage import default_storage
from django.http.response import HttpResponseRedirect, HttpResponse
from django.urls.base import reverse_lazy
from django.views.generic import ListView
from django.views.generic.base import TemplateView

from tesserae.apps.participant_app.models import Participant
from tesserae.apps.payment_app.models import Payment


class MyPaymentHistory(LoginRequiredMixin, UserPassesTestMixin, ListView):
    """
    List View to show a user's history of being paid
    """
    model = Payment
    template_name = 'payments/my_payment_history.html'

    def get_context_data(self, **kwargs):
        context = super(MyPaymentHistory, self).get_context_data(**kwargs)
        my_participant = Participant.objects.filter(gmail=self.request.user.email)
        context['my_payments'] = Payment.objects.filter(participant=my_participant).order_by('-payment_date')
        print(context['my_payments'])
        return context

    def test_func(self):
        # Testing that a user is not a staff member
        if self.request.user.is_staff:
            return False
        else:
            return True


class AllPaymentHistory(LoginRequiredMixin, UserPassesTestMixin, ListView):
    """
    List View to show all payment history
    """
    model = Payment
    template_name = 'payments/all_payment_history.html'

    def get_context_data(self, **kwargs):
        context = super(AllPaymentHistory, self).get_context_data(**kwargs)
        context['all_payments'] = Payment.objects.all().order_by('-payment_date')
        print(context['all_payments'])
        return context

    def test_func(self):
        return self.request.user.is_staff


class UploadPayment(LoginRequiredMixin, UserPassesTestMixin, TemplateView):
    """
    Template view for the staff compliance table showing all participants and their compliance values
    """
    template_name = 'payments/upload_payment.html'

    def test_func(self):
        return self.request.user.is_staff


@login_required
def upload_payment_file(request):
    """
    View to read and save the payment data from the CSV file.
    :param request: 
    :return: 
    """
    if request.method == 'POST' and request.FILES['payment_file']:

        filename = request.FILES['payment_file'].name  # received file name
        file_obj = request.FILES['payment_file']

        with default_storage.open('payment_files/' + filename, 'wb+') as destination:
            for chunk in file_obj.chunks():
                destination.write(chunk)

        with open('payment_files/' + filename, 'r+') as csvfile:
            reader = csv.DictReader(csvfile)
            for row in reader:
                try:
                    participant = Participant.objects.get(participant_id=row['participant'])

                    try:
                        date_paid = datetime.datetime.strptime(row['payment_date'], '%m/%d/%Y') #parser.parse(row['payment_date'], '%m-%d-%Y')
                    except ValueError:
                        date_paid = datetime.datetime.strptime(row['payment_date'], '%m-%d-%Y')

                    payment_obj, created = Payment.objects.update_or_create(
                        participant=participant,
                        payment_date=date_paid,
                        defaults={'payment_amount': row['payment_amount'],
                                  'payment_description': row['payment_description']}
                    )
                    payment_obj.save()
                except Exception as err:
                    print(type(err), err)
                    # If unsuccessful, go to the success page
                    failure_url = reverse_lazy('portal:payments:failure_upload_payment_file')
                    return HttpResponseRedirect(failure_url)

    # If successful, go to the success page
    success_url = reverse_lazy('portal:payments:success_upload_payment_file')
    return HttpResponseRedirect(success_url)


class UploadPaymentSuccess(LoginRequiredMixin, UserPassesTestMixin, TemplateView):
    """
    Template view for a successful payment upload
    
    """
    template_name = 'payments/success_upload_payment.html'

    def get_context_data(self, **kwargs):
        context = super(UploadPaymentSuccess, self).get_context_data(**kwargs)

        return context

    def test_func(self):
        return self.request.user.is_staff


class UploadPaymentFailure(LoginRequiredMixin, UserPassesTestMixin, TemplateView):
    """
    Template view for a failed payment upload
    
    """
    template_name = 'payments/failed_upload_payment.html'

    def get_context_data(self, **kwargs):
        context = super(UploadPaymentFailure, self).get_context_data(**kwargs)

        return context

    def test_func(self):
        return self.request.user.is_staff


@login_required
def download_payment_template(request):
    # Create the HttpResponse object with the appropriate CSV header.
    try:
        with open('payment_files/sample_payment_file.csv', 'r') as csvfile:
            csvreader = csv.reader(csvfile, delimiter=',')
            response = HttpResponse(content_type='text/csv')
            response['Content-Disposition'] = 'attachment; filename="sample_payment_file.csv"'

            writer = csv.writer(response)
            for line in csvreader:
                print(len(line))
                writer.writerow(line)

            # writer.close()
            csvfile.close()
            return response
    except IOError:
        return HttpResponse("Failed to open csv file")

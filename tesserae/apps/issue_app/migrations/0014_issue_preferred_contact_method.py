# -*- coding: utf-8 -*-
# Generated by Django 1.10.7 on 2017-09-07 14:46
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('issue_app', '0013_issuecontactmethod'),
    ]

    operations = [
        migrations.AddField(
            model_name='issue',
            name='preferred_contact_method',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, related_name='issue_contact_method', to='issue_app.IssueContactMethod'),
            preserve_default=False,
        ),
    ]

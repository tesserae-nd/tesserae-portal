from django.db import models


class Participant(models.Model):
    participant_id = models.CharField(max_length=32, unique=True, blank=False, null=False)
    gmail = models.CharField(max_length=128, blank=False, null=False)
    registration_date = models.DateField(auto_now_add=True)
    cohort = models.CharField(max_length=1024, blank=True, null=True)
    phone_platform = models.CharField(max_length=1024, blank=True, null=True)
    phone_version = models.CharField(max_length=100, blank=True, null=True)
    phone_number = models.CharField(max_length=50, blank=True, null=True)
    backend_participant_id = models.CharField(max_length=64, blank=True, null=True)

    def __str__(self):
        return self.participant_id

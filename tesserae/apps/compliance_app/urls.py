from django.conf.urls import url

from tesserae.apps.compliance_app.views import MySurveyComplianceList, MyWearableComplianceList, \
    MyPhoneAgentComplianceList, ComplianceList, ajax_compliance_list, UploadOverrides, \
    UploadComplianceOverridesSuccess, UploadComplianceOverridesFailure, upload_override_file, \
    get_participant_compliance_profile, get_participant_compliance_table, get_participant_compliance_details, \
    get_all_detailed_compliance_page, get_all_detailed_table, get_participant_detailed_compliance_page, \
    get_participant_detailed_table, request_compliance_refresh

urlpatterns = [
    url(r'^survey$', MySurveyComplianceList.as_view(), name='survey_compliance'),
    url(r'^wearable', MyWearableComplianceList.as_view(), name='wearable_compliance'),
    url(r'^phone-agent', MyPhoneAgentComplianceList.as_view(), name='phone_agent_compliance'),
    url(r'^staff-view', ComplianceList.as_view(), name='staff_compliance_table'),
    url(r'^ajax-staff-view', ajax_compliance_list, name='ajax_staff_compliance_table'),

    url(r'^upload-overrides-file/success$', UploadComplianceOverridesSuccess.as_view(),
        name='success_upload_overrides_file'),
    url(r'^upload-overrides-file/failure', UploadComplianceOverridesFailure.as_view(),
        name='failure_upload_overrides_file'),
    url(r'^upload-overrides-file$', upload_override_file, name='upload_overrides_file'),
    url(r'^upload-overrides', UploadOverrides.as_view(), name='upload_overrides'),
    url(r'^participant_profile/(?P<participant_id>[0-9]+)/', get_participant_compliance_profile,
        name='get_participant_compliance_profile'),
    url(r'^get_participant_compliance_table/(?P<participant_id>[0-9]+)/', get_participant_compliance_table,
        name='get_participant_compliance_table'),
    url(r'^get_detailed_compliance/(?P<participant_id>[0-9]+)/(?P<compliance_type>.+?)/(?P<date>[\w\-]+)/',
        get_participant_compliance_details, name='get_detailed_compliance'),
    url(r'^all_detailed_compliances/', get_all_detailed_compliance_page, name='get_all_detailed_compliance_page'),
    url(r'^get_all_detailed_table/', get_all_detailed_table, name='get_all_detailed_table'),

    url(r'^get_participant_daily_details/(?P<participant_id>[0-9]+)/(?P<major_stream>.+?)/',
        get_participant_detailed_compliance_page, name='get_participant_detailed_compliance_page'),
    url(r'^get_participant_details_table/', get_participant_detailed_table, name='get_participant_detailed_table'),
    url(r'^request_compliance_refresh/', request_compliance_refresh, name='request_compliance_refresh')
]

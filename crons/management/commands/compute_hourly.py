from django.core.management.base import BaseCommand, CommandError
import os
import zipfile
from tesserae.apps.participant_app.models import Participant
from tesserae.apps.compliance_app.models import DetailedCompliance
import csv
from django.conf import settings
import datetime
from tesserae.apps.reports_app.views import initialize_empty_day_array
import json
from tesserae.apps.compliance_app.models import ParticipantCompliance


class Command(BaseCommand):
    help = 'Precomputes hourly breakdowns'

    def add_arguments(self, parser):
        pass

    def handle(self, *args, **options):
        timing_start = datetime.datetime.now()
        csv_files = []

        # loop through all participants
        participants = Participant.objects.all().order_by('registration_date')
        total = len(participants)
        count = 0
        for participant in participants:
            count += 1
            print count, 'of', total
            # print "Working on participant:", participant.participant_id
            # start at their registration date
            current_date = participant.registration_date

            # go until today
            while current_date < datetime.datetime.today().date():
                # print "\t...on date:", current_date
                # for each, write a new CSV file (and keep track of it...)
                csv_name = 'Detail-' + participant.participant_id + '_' + current_date.strftime('%Y-%m-%d') + '.csv'
                with open(os.path.join('/tmp/', csv_name), 'w') as File:
                    writer = csv.writer(File)

                    # write the header
                    writer.writerow(['Date', 'Time', 'Wearable', 'Phone Agent - Battery',
                                     'Phone Agent - Streaming', 'Phone Agent - Beacons'])

                    # and the detailed compliance scores for phone agent
                    detailed_compliances = DetailedCompliance.objects.filter(date=current_date, participant=participant)
                    battery = initialize_empty_day_array()
                    streaming = initialize_empty_day_array()
                    beacons = initialize_empty_day_array()
                    wearable = initialize_empty_day_array()
                    for compliance in detailed_compliances:
                        if 'Battery Level' in compliance.detailed_type:
                            battery = json.loads(compliance.data)
                        if 'Garmin Streaming' in compliance.detailed_type:
                            streaming = json.loads(compliance.data)
                        if 'Beacon Sighting' in compliance.detailed_type:
                            beacons = json.loads(compliance.data)
                        if compliance.compliance_type.type_name == 'Wearable':
                            wearable = json.loads(compliance.data)

                    # loop through each "time"
                    current_time = datetime.datetime(current_date.year, current_date.month, current_date.day, 0, 0, 0)
                    indexer = 0
                    while current_time.date() < (current_date + datetime.timedelta(days=1)):
                        # print "\t\t...at:", current_time.strftime('%H:%M')
                        writer.writerow([current_time.strftime("%Y-%m-%d"), current_time.strftime('%H:%M'),
                                         wearable[indexer],
                                         battery[indexer],
                                         streaming[indexer],
                                         beacons[indexer]])

                        indexer += 1
                        current_time = current_time + datetime.timedelta(minutes=30)

                    current_date = current_date + datetime.timedelta(days=1)

                # writer.close()
                csv_files.append(csv_name)

        # now that they're all set...lets compress them
        zip_file_name = os.path.join('/tmp',
                                     'AllParticipantsHourly.zip')
        zfile = zipfile.ZipFile(zip_file_name, "w", allowZip64=True)

        # add each file to the zip
        for csv_file in csv_files:
            zfile.write(os.path.join('/tmp/', csv_file), os.path.basename(os.path.join('/tmp/', csv_file)))

            # then delete that file
            os.remove(os.path.join('/tmp/', csv_file))
        zfile.close()
        timing_end = datetime.datetime.now()

        self.stdout.write(self.style.SUCCESS(
            'Completed calculations in ['+str(timing_end - timing_start)+']'
        ))
        self.stdout.write(self.style.SUCCESS(
            'Zip file located at: /tmp/AllParticipantsHourly.zip'
        ))

        # timing_start = datetime.datetime.now()
        # csv_files = []
        #
        # # loop through all participants
        # participants = Participant.objects.all().order_by('registration_date')
        # for participant in participants:
        #     # for each, write a new CSV file (and keep track of it...)
        #     csv_name = 'Daily-' + participant.participant_id + '.csv'
        #     with open(os.path.join('/tmp/', csv_name), 'w') as File:
        #         writer = csv.writer(File)
        #
        #         # write the header
        #         writer.writerow(['Date', 'Overall', 'Survey', 'Wearable', 'Phone Agent', 'Phone Agent - Battery',
        #                          'Phone Agent - Streaming', 'Phone Agent - Beacons'])
        #
        #         # start at their registration date
        #         current_date = participant.registration_date
        #
        #         # go until today
        #         while current_date < datetime.datetime.today().date():
        #             # get scores
        #             wearable = 0.0
        #             survey = 0.0
        #             overall = 0.0
        #             phone_agent = 0.0
        #             scores = ParticipantCompliance.objects.filter(compliance_date=current_date, participant=participant)
        #             for score in scores:
        #                 if score.compliance_type.type_name == 'Survey':
        #                     survey = score.compliance_value
        #                 if score.compliance_type.type_name == 'Overall':
        #                     overall = score.compliance_value
        #                 if score.compliance_type.type_name == 'Wearable':
        #                     wearable = score.compliance_value
        #                 if score.compliance_type.type_name == 'Phone Agent':
        #                     phone_agent = score.compliance_value
        #
        #             # and the detailed compliance scores for phone agent
        #             detailed_compliances = DetailedCompliance.objects.filter(date=current_date, participant=participant)
        #             battery = 0.0
        #             streaming = 0.0
        #             beacons = 0.0
        #             for compliance in detailed_compliances:
        #                 if 'Battery Level' in compliance.detailed_type:
        #                     battery = (compliance.score * 100)
        #                 if 'Garmin Streaming' in compliance.detailed_type:
        #                     streaming = (compliance.score * 100)
        #                 if 'Beacon Sighting' in compliance.detailed_type:
        #                     beacons = (compliance.score * 100)
        #
        #             writer.writerow([current_date.strftime('%Y-%m-%d'), overall, survey, wearable, phone_agent, battery,
        #                              streaming, beacons])
        #             current_date = current_date + datetime.timedelta(days=1)
        #
        #         # writer.close()
        #         csv_files.append(csv_name)
        #
        # # now that they're all set...lets compress them
        # zip_file_basename = 'AllParticipantsDaily_' + datetime.datetime.now().strftime('%Y-%m-%d') + '.zip'
        # zip_file_name = os.path.join('/tmp',
        #                              zip_file_basename)
        # zfile = zipfile.ZipFile(zip_file_name, "w")
        #
        # # add each file to the zip
        # for csv_file in csv_files:
        #     zfile.write(os.path.join('/tmp/', csv_file), os.path.basename(os.path.join('/tmp/', csv_file)))
        #
        #     # then delete that file
        #     os.remove(os.path.join('/tmp/', csv_file))
        #
        # # add a note about when this was generated
        # timestamp_file = '/tmp/generated-'+datetime.datetime.now().strftime('%Y-%m-%dT%H:%M:%S')
        # fout = open(timestamp_file, 'w')
        # fout.close()
        # zfile.write(timestamp_file, os.path.basename(timestamp_file))
        # zfile.close()
        #
        # # now move the zip file to a standard location
        # os.rename(zip_file_name, '/tmp/AllParticipantsDaily.zip')
        # timing_end = datetime.datetime.now()
        #
        # self.stdout.write(self.style.SUCCESS(
        #     'Completed calculations in ['+str(timing_end - timing_start)+']'
        # ))
        # self.stdout.write(self.style.SUCCESS(
        #     'Zip file located at: /tmp/AllParticipantsDaily.zip'
        # ))

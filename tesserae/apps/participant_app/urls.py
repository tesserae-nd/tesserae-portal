from django.conf.urls import url

from tesserae.apps.participant_app.views import ParticipantCreate, ParticipantCreateSuccess

urlpatterns = [
    url(r'^register$', ParticipantCreate.as_view(), name='register_participant'),
    url(r'^register/success$', ParticipantCreateSuccess.as_view(), name='success_participant_create'),

]

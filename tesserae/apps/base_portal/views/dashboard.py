import json
from datetime import datetime, timedelta

from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.http.response import HttpResponse
from django.shortcuts import render
from django.views.generic import TemplateView

from tesserae.apps.compliance_app.compliance_calculations import compute_cumulative_compliance, \
    count_participants_last_30_days, \
    get_last_30_days, count_overall_compliant_last_30_days, last_30_days_average_compliance, \
    count_compliant_participants_last_30_days, get_next_30_days
from tesserae.apps.issue_app.models import Issue, IssueStatus
from tesserae.apps.participant_app.models import Participant
from tesserae.apps.payment_app.models import Payment
from crons.models import ChartDatum, Chart, ChartSeries


class ParticipantDashboardView(LoginRequiredMixin, UserPassesTestMixin, TemplateView):
    """
    Detail View to display status of compliance, issues, and payments to a participant
    """
    template_name = 'dashboard/participant_dashboard.html'

    def get_context_data(self, **kwargs):
        context = super(ParticipantDashboardView, self).get_context_data(**kwargs)
        my_reported_issues = Issue.objects.filter(reported_by_user=self.request.user).order_by('-date_reported')
        context['reported_issues'] = my_reported_issues
        issue_status = IssueStatus.objects.all().order_by('status')
        context['status_list'] = issue_status

        my_participant = Participant.objects.filter(gmail=self.request.user.email)

        # COMPLIANCE
        context['months_survey_compliance'] = compute_cumulative_compliance(my_participant, 'Survey', include_overrides=True)
        context['months_wearable_compliance'] = compute_cumulative_compliance(my_participant, 'Wearable', include_overrides=True)
        context['months_phone_agent_compliance'] = compute_cumulative_compliance(my_participant, 'Phone Agent', include_overrides=True)
        context['months_overall_compliance'] = (context['months_survey_compliance'] +  context['months_wearable_compliance'] + context['months_phone_agent_compliance']) / 3.0 #compute_cumulative_compliance(my_participant, 'Overall', include_overrides=True)

        context['current_month'] = datetime.today().date().strftime("%B")

        # PAYMENTS
        # only showing
        my_payments = Payment.objects.filter(participant=my_participant).order_by('-payment_date')[:5]
        context['my_payments'] = my_payments

        return context

    def test_func(self):
        return not self.request.user.is_staff


class StaffDashboardView(LoginRequiredMixin, UserPassesTestMixin, TemplateView):
    """
    Detail View to show issue details to a participant or personnel user
    """
    template_name = 'dashboard/staff_dashboard.html'

    def get_context_data(self, **kwargs):
        context = super(StaffDashboardView, self).get_context_data(**kwargs)
        my_assigned_issues = Issue.objects.filter(assigned_to=self.request.user).order_by('-date_reported')
        unassigned_issues = Issue.objects.filter(assigned_to=None).order_by('-date_reported')

        context['my_assigned_issues'] = my_assigned_issues
        context['unassigned_issues'] = unassigned_issues
        issue_status = IssueStatus.objects.all().order_by('status')
        context['status_list'] = issue_status

        # BEGIN CHART DATA

        # yesterday = datetime.today() - timedelta(1)
        # sorted_date_list = get_last_30_days(yesterday)
        # total_participant_count_list = count_participants_last_30_days(sorted_date_list)
        #
        # # participant compliant chart data
        #
        # context['total_participant_count'] = total_participant_count_list
        # context['yesterday'] = yesterday
        # context['yesterdays_total_participants'] = total_participant_count_list[29][1]
        #
        # compliant_participant_count_list = count_overall_compliant_last_30_days(sorted_date_list)
        # context['compliant_participant_count'] = compliant_participant_count_list
        # context['yesterdays_compliant_participants'] = compliant_participant_count_list[29][1]
        #
        # if total_participant_count_list[29][1]:
        #     yesterdays_percent_compliant = int((float(compliant_participant_count_list[29][1]) /
        #                                         total_participant_count_list[29][1]) * 100)
        # else:
        #     yesterdays_percent_compliant = 0
        #
        # context['yesterdays_percent_compliant'] = yesterdays_percent_compliant
        #
        # # average compliance chart data
        #
        # context['survey_average_list'] = last_30_days_average_compliance(sorted_date_list, 'Survey')
        # context['wearable_average_list'] = last_30_days_average_compliance(sorted_date_list, 'Wearable')
        # context['phoneagent_average_list'] = last_30_days_average_compliance(sorted_date_list, 'Phone Agent')
        # context['overall_average_list'] = last_30_days_average_compliance(sorted_date_list, 'Overall')
        #
        # # Compliance count by stream chart
        # compliant_survey_count_list = count_compliant_participants_last_30_days(sorted_date_list, 'Survey')
        # context['compliant_survey_count_list'] = compliant_survey_count_list
        # compliant_wearable_count_list = count_compliant_participants_last_30_days(sorted_date_list, 'Wearable')
        # context['compliant_wearable_count_list'] = compliant_wearable_count_list
        # compliant_phone_agent_count_list = count_compliant_participants_last_30_days(sorted_date_list, 'Phone Agent')
        # context['compliant_phone_agent_count_list'] = compliant_phone_agent_count_list
        #
        # date_start_ms = int(sorted_date_list[0].strftime("%s")) * 1000
        # date_end_ms = int(sorted_date_list[-1].strftime("%s")) * 1000
        #
        # context['date_start'] = date_start_ms
        # context['date_end'] = date_end_ms
        # print(context)
        return context

    def test_func(self):
        return self.request.user.is_staff


@login_required
def get_average_compliance_data(request):
    if not request.user.is_staff:
        return HttpResponse([])

    context = {}
    yesterday = datetime.today() - timedelta(1)
    sorted_date_list = get_last_30_days(yesterday)
    context['survey_average_list'] = get_homechart_data(sorted_date_list, 'Average Compliance', 'Survey') #last_30_days_average_compliance(sorted_date_list, 'Survey')
    context['wearable_average_list'] = get_homechart_data(sorted_date_list, 'Average Compliance', 'Wearable') #last_30_days_average_compliance(sorted_date_list, 'Wearable')
    context['phoneagent_average_list'] = get_homechart_data(sorted_date_list, 'Average Compliance', 'Phone Agent') #last_30_days_average_compliance(sorted_date_list, 'Phone Agent')
    context['overall_average_list'] = get_homechart_data(sorted_date_list, 'Average Compliance', 'Overall') #last_30_days_average_compliance(sorted_date_list, 'Overall')
    date_start_ms = int(sorted_date_list[0].strftime("%s")) * 1000
    date_end_ms = int(sorted_date_list[-1].strftime("%s")) * 1000

    context['date_start'] = date_start_ms
    context['date_end'] = date_end_ms
    return HttpResponse(json.dumps(context))


@login_required
def get_overall_compliance(request):
    if not request.user.is_staff:
        return HttpResponse([])

    context = {}
    yesterday = datetime.today() - timedelta(1)
    sorted_date_list = get_last_30_days(yesterday)
    total_participant_count_list = get_homechart_data(sorted_date_list, 'Overall Compliance', 'Total Participants') #count_participants_last_30_days(sorted_date_list)
    context['total_participant_count'] = total_participant_count_list
    compliant_participant_count_list = get_homechart_data(sorted_date_list, 'Overall Compliance', 'Compliant Participants') #count_overall_compliant_last_30_days(sorted_date_list)
    # print compliant_participant_count_list
    context['compliant_participant_count'] = compliant_participant_count_list

    context['yesterdays_compliant_participants'] = compliant_participant_count_list[-1][1]
    if total_participant_count_list[-1][1]:
        yesterdays_percent_compliant = int((float(compliant_participant_count_list[-1][1]) /
                                            total_participant_count_list[-1][1]) * 100)
    else:
        yesterdays_percent_compliant = 0
    context['yesterdays_total_participants'] = total_participant_count_list[-1][1]
    context['yesterdays_percent_compliant'] = yesterdays_percent_compliant
    date_start_ms = int(sorted_date_list[0].strftime("%s")) * 1000
    date_end_ms = int(sorted_date_list[-1].strftime("%s")) * 1000

    context['date_start'] = date_start_ms
    context['date_end'] = date_end_ms
    return HttpResponse(json.dumps(context))


def get_homechart_data(date_list, chart_name, stream_name):
    data = ChartDatum.objects.filter(chart_series__chart__name=chart_name,
                                     chart_series__name=stream_name,
                                     date__gte=date_list[0],
                                     date__lte=date_list[-1]).order_by('date')
    data_list = []
    for datum in data:
        data_list.append(
            [
                int(datum.date.strftime('%s')) * 1000,
                float(datum.value)
            ]
        )
    return data_list


@login_required
def get_stream_compliance(request):
    if not request.user.is_staff:
        return HttpResponse([])

    context = {}
    yesterday = datetime.today() - timedelta(1)
    sorted_date_list = get_last_30_days(yesterday)
    compliant_survey_count_list = get_homechart_data(sorted_date_list, 'Stream Compliance', 'Survey') #count_compliant_participants_last_30_days(sorted_date_list, 'Survey')
    context['compliant_survey_count_list'] = compliant_survey_count_list
    compliant_wearable_count_list = get_homechart_data(sorted_date_list, 'Stream Compliance', 'Wearable') #count_compliant_participants_last_30_days(sorted_date_list, 'Wearable')
    context['compliant_wearable_count_list'] = compliant_wearable_count_list
    compliant_phone_agent_count_list = get_homechart_data(sorted_date_list, 'Stream Compliance', 'Phone Agent') #count_compliant_participants_last_30_days(sorted_date_list, 'Phone Agent')
    context['compliant_phone_agent_count_list'] = compliant_phone_agent_count_list
    date_start_ms = int(sorted_date_list[0].strftime("%s")) * 1000
    date_end_ms = int(sorted_date_list[-1].strftime("%s")) * 1000

    context['date_start'] = date_start_ms
    context['date_end'] = date_end_ms
    return HttpResponse(json.dumps(context))


@login_required
def avg_stream_compliance_chart_ajax(request):
    """
    Ajax function for getting the average stream compliance chart data for the previous or next 30 days.
    Called on button click.
    :param request: 
    :return: 
    """
    # GET THE CORRECT 30 DAY PERIOD
    # Original start/end are the dates currently shown in the chart
    original_start = datetime.fromtimestamp(float(request.GET['start_date']) / 1000.0)
    original_end = datetime.fromtimestamp(float(request.GET['end_date']) / 1000.0)

    direction = request.GET['direction']

    # print(original_start)
    # print(original_end)

    if direction == "previous":
        # The new end will be the day before the original starting date ("yesterday" in a way)
        new_end_date = original_start - timedelta(1)
        # print("Now end on ", new_end_date)
        sorted_date_list = get_last_30_days(new_end_date)

    elif direction == "next":
        # The new start will be the day after the original ending date ("tomorrow" in a way)
        new_start_date = original_end + timedelta(1)
        # print("Now start on ", new_start_date)
        sorted_date_list = get_next_30_days(new_start_date)

    date_start_ms = int(sorted_date_list[0].strftime("%s")) * 1000
    date_end_ms = int(sorted_date_list[-1].strftime("%s")) * 1000

    # If the end of the 30 days is equal to/after yesterday, then disable the next 30 days button
    if sorted_date_list[-1] >= (datetime.today() - timedelta(1)).date():
        next_disabled = True
    else:
        next_disabled = False

    # GET THE DATA FOR THE 30 DAY PERIOD
    # average compliance chart data
    context_dictionary = {
        'survey_average_list': get_homechart_data(sorted_date_list, 'Average Compliance', 'Survey'),
        'wearable_average_list': get_homechart_data(sorted_date_list, 'Average Compliance', 'Wearable'),
        'phoneagent_average_list': get_homechart_data(sorted_date_list, 'Average Compliance', 'Phone Agent'),
        'overall_average_list': get_homechart_data(sorted_date_list, 'Average Compliance', 'Overall'),
        'date_start': date_start_ms,
        'date_end': date_end_ms,
        'next_disabled': next_disabled

    }
    return HttpResponse(json.dumps(context_dictionary), content_type="application/json")


@login_required
def overall_compliance_chart_ajax(request):
    """
    Ajax function for getting the overall compliance chart data for the next or previous 30 days.
    Called on button click.
    :param request: 
    :return: 
    """
    # GET THE CORRECT 30 DAY PERIOD
    # Original start/end are the dates currently shown in the chart
    original_start = datetime.fromtimestamp(float(request.GET['start_date']) / 1000.0)
    original_end = datetime.fromtimestamp(float(request.GET['end_date']) / 1000.0)

    direction = request.GET['direction']

    if direction == "previous":
        # The new end will be the day before the original starting date ("yesterday" in a way)
        new_end_date = original_start - timedelta(1)
        # print("Now end on ", new_end_date)
        sorted_date_list = get_last_30_days(new_end_date)

    elif direction == "next":
        # The new start will be the day after the original ending date ("tomorrow" in a way)
        new_start_date = original_end + timedelta(1)
        # print("Now start on ", new_start_date)
        sorted_date_list = get_next_30_days(new_start_date)

    date_start_ms = int(sorted_date_list[0].strftime("%s")) * 1000
    date_end_ms = int(sorted_date_list[-1].strftime("%s")) * 1000

    # If the end of the 30 days is equal to/after yesterday, then disable the next 30 days button
    if sorted_date_list[-1] >= (datetime.today() - timedelta(1)).date():
        next_disabled = True
    else:
        next_disabled = False

    # GET THE DATA FOR THE 30 DAY PERIOD
    total_participant_count_list = get_homechart_data(sorted_date_list, 'Overall Compliance', 'Total Participants') #count_participants_last_30_days(sorted_date_list)
    compliant_participant_count_list = get_homechart_data(sorted_date_list, 'Overall Compliance', 'Compliant Participants') #count_overall_compliant_last_30_days(sorted_date_list)

    # overall compliance chart data

    context_dictionary = {
        'total_participant_count': total_participant_count_list,
        'compliant_participant_count': compliant_participant_count_list,
        'date_start': date_start_ms,
        'date_end': date_end_ms,
        'next_disabled': next_disabled
    }

    return HttpResponse(json.dumps(context_dictionary), content_type="application/json")


@login_required
def stream_compliance_chart_ajax(request):
    """
    Ajax function for getting the stream compliance chart data for the next or previous 30 days.
    Called on button click.
    :param request: 
    :return: 
    """

    # GET THE CORRECT 30 DAY PERIOD
    # Original start/end are the dates currently shown in the chart
    original_start = datetime.fromtimestamp(float(request.GET['start_date']) / 1000.0)
    original_end = datetime.fromtimestamp(float(request.GET['end_date']) / 1000.0)

    direction = request.GET['direction']

    if direction == "previous":
        # The new end will be the day before the original starting date ("yesterday" in a way)
        new_end_date = original_start - timedelta(1)
        # print("Now end on ", new_end_date)
        sorted_date_list = get_last_30_days(new_end_date)

    elif direction == "next":
        # The new start will be the day after the original ending date ("tomorrow" in a way)
        new_start_date = original_end + timedelta(1)
        # print("Now start on ", new_start_date)
        sorted_date_list = get_next_30_days(new_start_date)

    date_start_ms = int(sorted_date_list[0].strftime("%s")) * 1000
    date_end_ms = int(sorted_date_list[-1].strftime("%s")) * 1000

    # If the end of the 30 days is equal to/after yesterday, then disable the next 30 days button
    if sorted_date_list[-1] >= (datetime.today() - timedelta(1)).date():
        next_disabled = True
    else:
        next_disabled = False

    # GET THE DATA FOR THE 30 DAY PERIOD
    compliant_survey_count_list = get_homechart_data(sorted_date_list, 'Stream Compliance', 'Survey') #count_compliant_participants_last_30_days(sorted_date_list, 'Survey')
    compliant_wearable_count_list = get_homechart_data(sorted_date_list, 'Stream Compliance', 'Wearable') #count_compliant_participants_last_30_days(sorted_date_list, 'Wearable')
    compliant_phone_agent_count_list = get_homechart_data(sorted_date_list, 'Stream Compliance', 'Phone Agent') #count_compliant_participants_last_30_days(sorted_date_list, 'Phone Agent')

    # overall compliance chart data

    context_dictionary = {
        'compliant_survey_count_list': compliant_survey_count_list,
        'compliant_wearable_count_list': compliant_wearable_count_list,
        'compliant_phone_agent_count_list': compliant_phone_agent_count_list,
        'date_start': date_start_ms,
        'date_end': date_end_ms,
        'next_disabled': next_disabled
    }

    return HttpResponse(json.dumps(context_dictionary), content_type="application/json")


@login_required
def participant_filter_issues_ajax(request):
    statuses = request.GET.getlist('selected_issue_statuses[]')
    status_objects = []
    for s in statuses:
        obj = IssueStatus.objects.get(status=s)
        status_objects.append(obj)

    filtered_issues = Issue.objects.filter(reported_by_user=request.user,
                                           status__in=status_objects).order_by('-date_reported')
    context_dictionary = {
        'issues_list': filtered_issues

    }
    return render(request, "dashboard/issues_list.html", context_dictionary)


@login_required
def staff_filter_issues_ajax(request):
    statuses = request.GET.getlist('selected_issue_statuses[]')
    status_objects = []
    for s in statuses:
        obj = IssueStatus.objects.get(status=s)
        status_objects.append(obj)

    filtered_issues = Issue.objects.filter(assigned_to=request.user,
                                           status__in=status_objects).order_by('-date_reported')
    context_dictionary = {
        'issues_list': filtered_issues

    }
    return render(request, "dashboard/issues_list.html", context_dictionary)


@login_required
def staff_filter__unassigned_issues_ajax(request):
    statuses = request.GET.getlist('selected_issue_statuses[]')
    status_objects = []
    for s in statuses:
        obj = IssueStatus.objects.get(status=s)
        status_objects.append(obj)

    filtered_issues = Issue.objects.filter(assigned_to=None,
                                           status__in=status_objects).order_by('-date_reported')
    context_dictionary = {
        'issues_list': filtered_issues

    }
    return render(request, "dashboard/issues_list.html", context_dictionary)

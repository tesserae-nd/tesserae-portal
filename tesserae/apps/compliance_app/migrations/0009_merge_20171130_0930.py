# -*- coding: utf-8 -*-
# Generated by Django 1.10.7 on 2017-11-30 14:30
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('compliance_app', '0008_complianceoverride'),
        ('compliance_app', '0008_participantcompliance_calculated_on'),
    ]

    operations = [
    ]

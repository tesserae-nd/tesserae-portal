import re
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit
from django import forms

from tesserae.apps.participant_app.models import Participant


class NewParticipantForm(forms.ModelForm):
    """
    This form is used for staff members to register new participants
    """

    def __init__(self, *args, **kwargs):
        super(NewParticipantForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_show_errors = False
        self.helper.add_input(Submit('submit', 'Add Participant', css_class='btn-primary pull-right'))

    participant_id = forms.CharField(label='Participant ID')

    class Meta:
        model = Participant
        fields = [
            'participant_id',
            'gmail'
        ]

    def clean(self):
        print("cleaning...........")
        print(self.cleaned_data)

        error_messages = []

        # validate participant_id
        if Participant.objects.filter(participant_id=self.cleaned_data.get('participant_id')).exists():
            error_messages.append('Participant ID is not unique')
            self._errors["participant_id"] = "Participant with this ID already exists! " \
                                             "Check that Participant ID is entered correctly."

        # validate gmail
        if self.cleaned_data.get('gmail'):
            match = re.search('^(\w+(\.*\w+)*)(@{1})(\w+\.{1}\w+)$', self.cleaned_data.get('gmail'))
            if not match:
                error_messages.append('Gmail must be a valid email address')
                self._errors["gmail"] = "Verify that the email address is entered correctly"

        return self.cleaned_data


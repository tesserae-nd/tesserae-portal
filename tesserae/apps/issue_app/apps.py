from django.apps import AppConfig


class IssueAppConfig(AppConfig):
    name = 'tesserae.apps.issue_app'

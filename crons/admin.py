from django.contrib import admin

# Register your models here.
from crons.models import Chart, ChartDatum, ChartSeries


@admin.register(Chart)
class ChartAdmin(admin.ModelAdmin):
    list_display = ('name',)


@admin.register(ChartSeries)
class ChartSeriesAdmin(admin.ModelAdmin):
    list_display = ('name',)


@admin.register(ChartDatum)
class ChartDatumAdmin(admin.ModelAdmin):
    list_display = ('date', 'value',)


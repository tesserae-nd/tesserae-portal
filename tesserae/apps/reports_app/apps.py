from django.apps import AppConfig


class ReportsAppConfig(AppConfig):
    name = 'tesserae.apps.reports_app'

from datetime import datetime, timedelta
from time import strptime

from tesserae.apps.users.models import User

from django.core.exceptions import ObjectDoesNotExist

from tesserae.apps.compliance_app.models import ParticipantCompliance, ComplianceOverride
from tesserae.apps.participant_app.models import Participant

from django.db.models import Sum
from collections import namedtuple


def compute_cumulative_compliance(participant, stream, include_overrides=False):
    """
    Function to compute the cumulative compliance over the course of the current month.
    Average all compliances for the given stream over
    :param participant: 
    :param stream:
    :param include_overrides:
    :return: month_to_date_compliance
    """

    # get the participant's start date from the user object
    # if type(participant) is Participant:
    #     part = participant
    # else:
    #     part = participant[0]
    # portal_user = User.objects.filter(email=part.gmail)
    #
    # if portal_user is None or len(portal_user) == 0:
    #     start_date = datetime(1970, 1, 1)
    # else:
    #     start_date = portal_user[0].date_joined + timedelta(days=1)
    if type(participant) is Participant:
        start_date = participant.registration_date
    else:
        start_date = participant[0].registration_date
    start_date = start_date + timedelta(days=1)

    compliances = ParticipantCompliance.objects.filter(participant=participant,
                                                           compliance_type__type_name=stream,
                                                           compliance_date__gte=start_date,
                                                           compliance_date__lte=datetime.today() - timedelta(days=1))\
        .order_by(
        '-compliance_date')

    compliance_list = []
    if include_overrides:
        print("INCLUDE OVERRIDES")
        for compliance in compliances:
            print(compliance)
            override = ComplianceOverride.objects.filter(participant=compliance.participant,
                                                         compliance_type=compliance.compliance_type,
                                                         compliance_override_date=compliance.compliance_date)
            value = compliance.compliance_value
            if len(override) > 0:
                value = override[0].compliance_override_value
                print("Found override: ")
                print(override)
            new_compliance = namedtuple('ComplianceObj', 'compliance_type, compliance_date, compliance_value')
            new_compliance.compliance_date = compliance.compliance_date
            new_compliance.compliance_type = compliance.compliance_type
            new_compliance.compliance_value = value
            compliance_list.append(new_compliance)

    sum_of_all_this_months_compliance, number_of_days_in_month_so_far = get_monthly_compliance_category_total(
        compliance_list)
    #
    # print "This month:", sum_of_all_this_months_compliance
    # print "Number of days this month:", number_of_days_in_month_so_far
    # print "==="

    if number_of_days_in_month_so_far > 0:
        month_to_date_compliance = int(round(sum_of_all_this_months_compliance / number_of_days_in_month_so_far))
    else:
        month_to_date_compliance = 0

    return month_to_date_compliance


def get_monthly_compliance_category_total(compliance_list):
    """
    Function returns the monthly cumulative compliance (to-date) for a participant and the number of days to date 
    in the current month
    :param compliance_list: 
    :return: 
    """
    month_total = 0
    number_of_days = 0
    # For each compliance entry in the current month, sum all compliance values
    for compliance_entry in compliance_list:
        # if compliance_entry.compliance_date.month == datetime.today().date().month:
        number_of_days += 1
        month_total += compliance_entry.compliance_value
    return month_total, number_of_days


def count_overall_compliant_participants():
    """
    Function to determine how many of the participants are overall compliant
    :return: overall: dictionary containing number of compliant participants and number of noncompliant participants
    """
    participant_users = Participant.objects.all()

    total_participant_count = participant_users.count()
    overall_compliant_participants = 0
    overall_compliant_people = []

    for participant in participant_users:
        month_to_date_overall_compliance = compute_cumulative_compliance(participant, 'Overall')

        if month_to_date_overall_compliance >= 80:
            overall_compliant_participants += 1
            overall_compliant_people.append(participant)

    overall = {
        'compliant': overall_compliant_participants,
        'noncompliant': total_participant_count - overall_compliant_participants
    }

    return overall


def get_last_30_days(yesterday):
    """
    Function to return a list of the last 30 dates. Most recent date will be yesterday.
    :param yesterday: 
    :return: 
    """
    date_list = []
    for x in range(0, 30):
        date_list.append((yesterday - timedelta(x)).date())

    sorted_date_list = sorted(date_list)

    return sorted_date_list


def get_next_30_days(startdate):
    """
    Function to return a list of the next 30 dates.
    :param startdate: 
    :return: 
    """
    date_list = []

    enddate = startdate + timedelta(30)
    # print(startdate)
    # print(enddate)

    # for 30 dates
    for x in range(0, 30):
        date_list.append((startdate + timedelta(x)).date())

    sorted_date_list = sorted(date_list)

    return sorted_date_list


def get_date_list(start_date, end_date):
    """
    Function to return a list of the dates between the start and end dates provided.
    :param start_date:
    :param end_date: 
    :return: 
    """
    date_list = []

    delta = datetime.strptime(end_date, "%B %d, %Y") - datetime.strptime(start_date, "%B %d, %Y")

    for i in range(delta.days + 1):
        date = datetime.strptime(start_date, "%B %d, %Y") + timedelta(days=i)
        date_list.append(date)

    sorted_date_list = sorted(date_list)
    return sorted_date_list


def count_participants_last_30_days(sorted_date_list):
    """
    Function to determine how many people total were registered as participants in the study every day 
    for the last 30 days.

    :param: yesterday
    :return: total_participant_count_list - list of lists. 
             each list in the list contains the date and number of participants registered as of that day.
    """

    total_participant_count_list = []

    for date in sorted_date_list:
        date_object = int(date.strftime("%s")) * 1000  # highcharts expects dates in milliseconds
        daily_participant_count = Participant.objects.filter(registration_date__lte=date).count()
        total_participant_count_list.append([date_object, daily_participant_count])

    return total_participant_count_list


def count_overall_compliant_last_30_days(sorted_date_list):
    """
    Function counts the number of registered participants who met the criteria to be considered compliant 
    for every day in the last 30 days.
    :param sorted_date_list: 
    :return: 
    """
    # Get all participants
    participant_users = Participant.objects.all()

    # List of dates and number of compliant participants
    compliant_participants_count_list = []

    # For each of the last 30 days
    # if the participant was registered before the date
    # determine if they were compliant on that date
    for date in sorted_date_list:
        # How many participants were compliant each day
        daily_compliant_participant_count = 0

        date_object = int(date.strftime("%s")) * 1000  # highcharts expects dates in milliseconds
        for participant in participant_users:
            if participant.registration_date <= date:
                cumulative_overall_compliance = ParticipantCompliance.objects.filter(participant=participant,
                                                                                  compliance_date=date,
                                                                                  compliance_type__type_name='Overall')
                if len(cumulative_overall_compliance) != 0:
                    # print cumulative_overall_compliance
                    if cumulative_overall_compliance[0].compliance_value >= 80:
                        daily_compliant_participant_count += 1

        compliant_participants_count_list.append([date_object, daily_compliant_participant_count])

    return compliant_participants_count_list


def last_30_days_average_compliance(sorted_date_list, stream):
    """
    Function to compute the average compliance for a given stream over the last 30 days.
    Returns a list of lists, where each inner list is a date (in milliseconds) and an average compliance value.
    :param sorted_date_list: 
    :param stream: 
    :return: 
    """
    # Get all participants
    participant_users = Participant.objects.all()

    # List of dates and number of compliant participants
    average_compliance_list = []

    # For each of the last 30 days
    # if the participant was registered before the date
    # add their compliance to the list
    for date in sorted_date_list:
        date_object = int(date.strftime("%s")) * 1000  # highcharts expects dates in milliseconds
        date_participant_count = 0
        date_compliance_sum = 0
        for participant in participant_users:

            if participant.registration_date <= date:
                date_participant_count += 1

            date_participant_stream_compliance = ParticipantCompliance.objects.filter(participant=participant,
                                                                                      compliance_date=date,
                                                                                      compliance_type__type_name=stream)

            if date_participant_stream_compliance:
                date_compliance_sum += date_participant_stream_compliance[0].compliance_value

        if date_participant_count > 0:
            date_average_compliance = date_compliance_sum / date_participant_count
        else:
            date_average_compliance = 0
        average_compliance_list.append([date_object, date_average_compliance])
    return average_compliance_list


def count_compliant_participants_last_30_days(sorted_date_list, stream):
    """
    Function to count how many participants are compliant for a given stream in the last 30 days
    :param sorted_date_list: 
    :param stream: 
    :return: 
    """

    # Get all participants
    participant_users = Participant.objects.all()
    # total_participant_count = participant_users.count()

    # List of dates and number of compliant participants
    compliant_participants_count_list = []

    # For each of the last 30 days
    # if the participant was registered before the date
    # determine if they were compliant on that date for the specified stream
    for date in sorted_date_list:
        # How many participants were compliant each day
        daily_compliant_participant_count = 0

        date_object = int(date.strftime("%s")) * 1000  # highcharts expects dates in milliseconds

        # For each participant, check registration date
        # Then get the stream compliance for the date and check if greater or equal to 80%
        for participant in participant_users:
            if participant.registration_date <= date:
                try:
                    daily_stream_compliance = ParticipantCompliance.objects.get(participant=participant,
                                                                                compliance_type__type_name=stream,
                                                                                compliance_date=date)
                    if daily_stream_compliance.compliance_value >= 80:
                        daily_compliant_participant_count += 1
                except ObjectDoesNotExist:
                    pass

        compliant_participants_count_list.append([date_object, daily_compliant_participant_count])

    return compliant_participants_count_list


def compute_cumulative_compliance_over_date_range(sorted_date_list, participant, stream):
    """
    Function to compute the cumulative compliance over the course of the given date list.
    Average all compliance for the given stream over the given date list (default 30 days)
    :param sorted_date_list: 
    :param participant: 
    :param stream: 
    :return: month_to_date_compliance
    """
    # print("In compliance calculations for participant: ", participant, stream)
    # print("Overrides are OFF")

    # adjust the start date filter parameter in case their registration date is after this filter

    for i in range(0, len(sorted_date_list)):
        if type(sorted_date_list[i]) == datetime:
            sorted_date_list[i] = sorted_date_list[i].date()
    start_date = sorted_date_list[0]
    if start_date < participant.registration_date:
        start_date = (participant.registration_date + timedelta(days=1))

    # compute the compliance value sum
    compliance_sum = ParticipantCompliance.objects.filter(participant=participant,
                                                          compliance_date__gte=start_date,
                                                          compliance_date__lte=sorted_date_list[-1],
                                                          compliance_type__type_name=stream)\
        .aggregate(Sum('compliance_value'))
    # if it's empty, set it to zero
    if compliance_sum is None or compliance_sum['compliance_value__sum'] is None:
        compliance_sum = 0.0
    else:
        compliance_sum = compliance_sum['compliance_value__sum']

    # compute total days, but add 1 because of how Python handles date subtraction
    # (excludes the last day due to assuming time is at midnight)
    total_days = (sorted_date_list[-1] - start_date).days + 1

    # also, check if there's an override for this date/category
    override = ComplianceOverride.objects.filter(participant=participant, compliance_override_date=start_date,
                                                 compliance_type__type_name=stream)
    if len(override) > 0:
        override = override[0].compliance_override_value
        print(override)
    else:
        override = None

    # return the average!
    if total_days == 0:
        return {
            'score': 0.0,
            'override': override
        }



    return {
        'score': float(compliance_sum) / float(int(total_days)),
        'override': override
    }


def compute_cumulative_compliance_over_date_range_with_overrides(sorted_date_list, participant, stream):
    """
    Function to compute the cumulative compliance over the course of the given date list taking into account overrides.
    Average all compliance for the given stream over the given date list (default 30 days)
    :param sorted_date_list: 
    :param participant: 
    :param stream: 
    :return: month_to_date_compliance
    """
    # print("In compliance calculations for participant: ", participant, stream)
    # print("Overrides are ON")

    compliance_total = 0

    number_of_days = 0

    # adjust the start date filter parameter in case their registration date is after this filter
    start_date = sorted_date_list[0]

    if start_date < participant.registration_date:
        start_date = (participant.registration_date + timedelta(days=1))

    # gather a list (not aggregate sum this time, because we need to know which ones to override), of compliance values
    compliances = {}
    compliance_values = ParticipantCompliance.objects.filter(participant=participant,
                                                             compliance_date__gte=start_date,
                                                             compliance_date__lte=sorted_date_list[-1],
                                                             compliance_type__type_name=stream)
    for c in compliance_values:
        compliances[c.compliance_date.strftime('%Y-%m-%d')] = c.compliance_value

    # and get a list of overrides
    overrides = {}
    override_list = ComplianceOverride.objects.filter(participant=participant,
                                                compliance_override_date__gte=start_date,
                                                compliance_override_date__lte=sorted_date_list[-1],
                                                compliance_type__type_name=stream)
    for o in override_list:
        overrides[o.compliance_override_date.strftime('%Y-%m-%d')] =o.compliance_override_value

    current_date = start_date
    while current_date <= sorted_date_list[-1]:
        c = 0.0
        if current_date.strftime('%Y-%m-%d') in overrides:
            c = overrides[current_date.strftime('%Y-%m-%d')]
        else:
            if current_date.strftime('%Y-%m-%d') in compliances:
                c = compliances[current_date.strftime('%Y-%m-%d')]

        compliance_total += c
        current_date = current_date + timedelta(days=1)
        number_of_days += 1

    # for date in sorted_date_list:
    #     try:
    #         # See if there is an override compliance score for this date
    #         override_compliance = ComplianceOverride.objects.get(participant=participant,
    #                                                              compliance_override_date=date,
    #                                                              compliance_type__type_name=stream)
    #         compliance_total += override_compliance.compliance_override_value
    #         number_of_days += 1
    #     except ObjectDoesNotExist:
    #         # If no override compliance score exists, check if there is a regular compliance score
    #         try:
    #             date_compliance = ParticipantCompliance.objects.get(participant=participant,
    #                                                                 compliance_date=date,
    #                                                                 compliance_type__type_name=stream)
    #             compliance_total += date_compliance.compliance_value
    #             number_of_days += 1
    #         except ObjectDoesNotExist:
    #             # If there is no compliance data for a date, skip that date
    #             pass
    #
    # if number_of_days > 0:
    #     average_compliance = float(compliance_total) / number_of_days
    # else:
    #     average_compliance = 0
    if number_of_days == 0:
        return 0.0
    return float(compliance_total) / float(number_of_days)

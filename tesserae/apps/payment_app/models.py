from django.db import models

from tesserae.apps.participant_app.models import Participant


# Create your models here.


class Payment(models.Model):
    participant = models.ForeignKey(Participant, related_name='participant_paid', blank=False, null=False)
    payment_date = models.DateTimeField()
    payment_amount = models.DecimalField(decimal_places=2, max_digits=7)
    payment_description = models.CharField(max_length=2048, default="Payment")

    def __str__(self):
        return 'Paid %s $%.2f on %s' % (self.participant, self.payment_amount, self.payment_date)

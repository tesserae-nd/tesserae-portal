from django.db import models

from tesserae.apps.participant_app.models import Participant
# Create your models here.


class ComplianceType(models.Model):
    type_name = models.CharField(max_length=20, blank=False, null=False)

    def __str__(self):
        return self.type_name


class ParticipantCompliance(models.Model):
    participant = models.ForeignKey(Participant, related_name='participant', to_field='participant_id', blank=True, null=True)
    compliance_date = models.DateField(blank=False, null=False)
    compliance_value = models.PositiveIntegerField(blank=False, null=False)
    compliance_type = models.ForeignKey('ComplianceType', related_name="compliance_type", blank=False, null=False)
    calculated_on = models.DateTimeField(auto_created=True, auto_now=True)

    def __str__(self):
        return '%s -- %s : %i (%s)' % (self.participant.id, self.compliance_date, self.compliance_value,
                                       self.compliance_type)


# Model for compliance overrides
class ComplianceOverride(models.Model):
    participant = models.ForeignKey(Participant, related_name='override_participant', to_field='participant_id', blank=True, null=True)
    compliance_override_date = models.DateField(blank=False, null=False)
    compliance_override_value = models.PositiveIntegerField(blank=False, null=False)
    compliance_type = models.ForeignKey('ComplianceType', related_name="compliance_override_type", blank=False, null=False)

    def __str__(self):
        return 'Override for %s -- %s : %i (%s)' % (self.participant.id, self.compliance_override_date,
                                                    self.compliance_override_value, self.compliance_type)


class DetailedCompliance(models.Model):
    participant = models.ForeignKey(Participant, related_name='detailed_compliance', to_field='participant_id')
    date = models.DateField()
    score = models.DecimalField(max_digits=6, decimal_places=4)
    data = models.CharField(max_length=144, null=True)
    compliance_type = models.ForeignKey(ComplianceType, related_name='detailed_type')
    detailed_type = models.CharField(max_length=1024)
from django.apps import AppConfig


class StaffAppConfig(AppConfig):
    name = 'tesserae.apps.staff_app'

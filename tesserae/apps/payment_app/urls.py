from django.conf.urls import url

from tesserae.apps.payment_app.views import MyPaymentHistory, UploadPayment, AllPaymentHistory, upload_payment_file, \
    UploadPaymentSuccess, UploadPaymentFailure, download_payment_template

urlpatterns = [
    url(r'^my-payment-history$', MyPaymentHistory.as_view(), name='payment_history'),
    url(r'^upload-payment-file/success$', UploadPaymentSuccess.as_view(), name='success_upload_payment_file'),
    url(r'^upload-payment-file/failure', UploadPaymentFailure.as_view(), name='failure_upload_payment_file'),
    url(r'^upload-payment-file$', upload_payment_file, name='upload_payment_file'),
    url(r'^upload-payment$', UploadPayment.as_view(), name='upload_payment'),
    url(r'^payment-history$', AllPaymentHistory.as_view(), name='all_payment_history'),
    url(r'^template-download$', download_payment_template, name='payment_template_download'),

]

from django.contrib import admin
from tesserae.apps.compliance_app.models import ComplianceType, ParticipantCompliance, ComplianceOverride

# Register your models here.

admin.site.register(ComplianceType)
# admin.site.register(ParticipantCompliance)


@admin.register(ParticipantCompliance)
class ParticipantComplianceAdmin(admin.ModelAdmin):
    list_display = ('participant', 'compliance_type', 'compliance_date', 'compliance_value')


@admin.register(ComplianceOverride)
class ComplianceOverrideAdmin(admin.ModelAdmin):
    list_display = ('participant', 'compliance_override_date', 'compliance_type', 'compliance_override_value')

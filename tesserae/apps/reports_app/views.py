from django.http import HttpResponseRedirect
from django.http.response import HttpResponse, JsonResponse
from django.shortcuts import render
import csv
import datetime
from tesserae.apps.participant_app.models import Participant
from tesserae.apps.compliance_app.models import ParticipantCompliance, DetailedCompliance, ComplianceOverride
from tesserae.apps.issue_app.models import Issue
import json
import os
import zipfile
from wsgiref.util import FileWrapper
from django.contrib.admin.views.decorators import staff_member_required


@staff_member_required
def reports_home(request):
    print request.user.groups.all()
    group_names = []
    for group in request.user.groups.all():
        group_names.append(group.name)
    return render(request, '../templates/reports/reports_home.html', {'groups': group_names})


@staff_member_required
def download_all_csv(request):
    # Create the HttpResponse object with the appropriate CSV header.
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="All Participants - %(date)s.csv"' \
                                      % {'date': datetime.datetime.now().strftime('%Y-%m-%d')}

    writer = csv.writer(response)
    writer.writerow(['Portal ID', 'Email', 'Enrollment Date', 'Cohort', 'Phone Type', 'Phone Version', 'Phone Number'])

    # gather a list of all participants
    participants = Participant.objects.all().order_by('registration_date')
    for participant in participants:
        writer.writerow([
            participant.participant_id,
            participant.gmail,
            participant.registration_date,
            participant.cohort,
            participant.phone_platform,
            participant.phone_version,
            participant.phone_number
        ])

    return response


@staff_member_required
def download_issues(request):
    # Create the HttpResponse object with the appropriate CSV header.
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="All Participants - %(date)s.csv"' \
                                      % {'date': datetime.datetime.now().strftime('%Y-%m-%d')}

    writer = csv.writer(response)
    writer.writerow(['Participant ID', 'Issue Category', 'Issue Number', 'Original Issue Posted by Participant', 'Original Issue Post Date', 'Notes entered by Tesserae Team Member'])

    # gather a list of all participants
    issues = Issue.objects.all().order_by('category__name', 'date_reported')
    for issue in issues:
        issue_notes = issue.note_issue.all()
        notes = None
        if len(issue_notes) > 0:
            notes = ''
            for note in issue_notes:
                notes += note.note + '\n'
            notes = notes.encode('utf-8')

        writer.writerow([
            '"'+str(issue.reported_by_participant.participant_id)+'"',
            issue.category.name,
            issue.id,
            (issue.subject + ':\n' + issue.description).encode('utf-8'),
            issue.date_reported.strftime('%Y-%m-%d %H:%M'),
            notes
        ])

    return response


@staff_member_required
def download_daily(request):
    csv_files = []

    # loop through all participants
    participants = Participant.objects.all().order_by('registration_date')
    for participant in participants:
        # for each, write a new CSV file (and keep track of it...)
        csv_name = 'Daily-'+participant.participant_id+'.csv'
        with open(os.path.join('/tmp/hourly/', csv_name), 'w') as File:
            writer = csv.writer(File)

            # write the header
            writer.writerow(['Date', 'Overall', 'Survey', 'Wearable', 'Phone Agent', 'Phone Agent - Battery',
                             'Phone Agent - Streaming', 'Phone Agent - Beacons'])

            # start at their registration date
            current_date = participant.registration_date

            # go until today
            while current_date < datetime.datetime.today().date():
                # get scores
                wearable = 0.0
                survey = 0.0
                overall = 0.0
                phone_agent = 0.0
                scores = ParticipantCompliance.objects.filter(compliance_date=current_date, participant=participant)
                for score in scores:
                    if score.compliance_type.type_name == 'Survey':
                        survey = score.compliance_value
                    if score.compliance_type.type_name == 'Overall':
                        overall = score.compliance_value
                    if score.compliance_type.type_name == 'Wearable':
                        wearable = score.compliance_value
                    if score.compliance_type.type_name == 'Phone Agent':
                        phone_agent = score.compliance_value

                # and the detailed compliance scores for phone agent
                detailed_compliances = DetailedCompliance.objects.filter(date=current_date, participant=participant)
                battery = 0.0
                streaming = 0.0
                beacons = 0.0
                for compliance in detailed_compliances:
                    if 'Battery Level' in compliance.detailed_type:
                        battery = (compliance.score * 100)
                    if 'Garmin Streaming' in compliance.detailed_type:
                        streaming = (compliance.score * 100)
                    if 'Beacon Sighting' in compliance.detailed_type:
                        beacons = (compliance.score * 100)

                writer.writerow([current_date.strftime('%Y-%m-%d'), overall, survey, wearable, phone_agent, battery,
                                 streaming, beacons])
                current_date = current_date + datetime.timedelta(days=1)

            # writer.close()
            csv_files.append(csv_name)

    # now that they're all set...lets compress them
    zip_file_name = os.path.join('/tmp', 'AllParticipantsDaily_'+datetime.datetime.now().strftime('%Y-%m-%d')+'.zip')
    zfile = zipfile.ZipFile(zip_file_name, "w", allowZip64=True)

    # add each file to the zip
    for csv_file in csv_files:
        zfile.write(os.path.join('/tmp/', csv_file), os.path.basename(os.path.join('/tmp/', csv_file)))

        # then delete that file
        os.remove(os.path.join('/tmp/', csv_file))
    zfile.close()

    response = HttpResponse(FileWrapper(file(zip_file_name, 'rb')), content_type='application/zip')
    response['Content-Disposition'] = 'attachment; filename=' + zip_file_name.replace("/tmp/", "")

    return response


def initialize_empty_day_array():
    arr = []
    for i in range(0, 48):
        arr.append(0)
    return arr


@staff_member_required
def download_hourly(request):
    response = HttpResponse(FileWrapper(file('/tmp/AllParticipantsHourly.zip', 'rb')), content_type='application/zip')
    response['Content-Disposition'] = 'attachment; filename=AllParticipantsHourly.zip'
    return response

    # csv_files = []
    #
    # # loop through all participants
    # participants = Participant.objects.all().order_by('registration_date')
    # for participant in participants:
    #     print participant
    #     # print "Working on participant:", participant.participant_id
    #     # start at their registration date
    #     current_date = participant.registration_date
    #
    #     # go until today
    #     while current_date < datetime.datetime.today().date():
    #         # print "\t...on date:", current_date
    #         # for each, write a new CSV file (and keep track of it...)
    #         csv_name = 'Detail-'+participant.participant_id+'_'+current_date.strftime('%Y-%m-%d')+'.csv'
    #         with open(os.path.join('/tmp/', csv_name), 'w') as File:
    #             writer = csv.writer(File)
    #
    #             # write the header
    #             writer.writerow(['Date', 'Time', 'Wearable', 'Phone Agent - Battery',
    #                              'Phone Agent - Streaming', 'Phone Agent - Beacons'])
    #
    #             # and the detailed compliance scores for phone agent
    #             detailed_compliances = DetailedCompliance.objects.filter(date=current_date, participant=participant)
    #             battery = initialize_empty_day_array()
    #             streaming = initialize_empty_day_array()
    #             beacons = initialize_empty_day_array()
    #             wearable = initialize_empty_day_array()
    #             for compliance in detailed_compliances:
    #                 if 'Battery Level' in compliance.detailed_type:
    #                     battery = json.loads(compliance.data)
    #                 if 'Garmin Streaming' in compliance.detailed_type:
    #                     streaming = json.loads(compliance.data)
    #                 if 'Beacon Sighting' in compliance.detailed_type:
    #                     beacons = json.loads(compliance.data)
    #                 if compliance.compliance_type.type_name == 'Wearable':
    #                     wearable = json.loads(compliance.data)
    #
    #             # loop through each "time"
    #             current_time = datetime.datetime(current_date.year, current_date.month, current_date.day, 0, 0, 0)
    #             indexer = 0
    #             while current_time.date() < (current_date + datetime.timedelta(days=1)):
    #                 # print "\t\t...at:", current_time.strftime('%H:%M')
    #                 writer.writerow([current_time.strftime("%Y-%m-%d"), current_time.strftime('%H:%M'),
    #                                  wearable[indexer],
    #                                  battery[indexer],
    #                                  streaming[indexer],
    #                                  beacons[indexer]])
    #
    #                 indexer += 1
    #                 current_time = current_time + datetime.timedelta(minutes=30)
    #
    #             current_date = current_date + datetime.timedelta(days=1)
    #
    #         # writer.close()
    #         csv_files.append(csv_name)
    #
    # # now that they're all set...lets compress them
    # zip_file_name = os.path.join('/tmp', 'AllParticipantsDaily_'+datetime.datetime.now().strftime('%Y-%m-%d')+'.zip')
    # zfile = zipfile.ZipFile(zip_file_name, "w")
    #
    # # add each file to the zip
    # for csv_file in csv_files:
    #     zfile.write(os.path.join('/tmp/', csv_file), os.path.basename(os.path.join('/tmp/', csv_file)))
    #
    #     # then delete that file
    #     os.remove(os.path.join('/tmp/', csv_file))
    # zfile.close()
    #
    # response = HttpResponse(FileWrapper(file(zip_file_name, 'rb')), content_type='application/zip')
    # response['Content-Disposition'] = 'attachment; filename=' + zip_file_name.replace("/tmp/", "")
    #
    # return response


@staff_member_required
def get_all_overrides(request):
    start = datetime.datetime.now()
    print "Starting now:", start

    # Create the HttpResponse object with the appropriate CSV header.
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="All Overrides - %(date)s.csv"' \
                                      % {'date': datetime.datetime.now().strftime('%Y-%m-%d')}

    writer = csv.writer(response)

    # write the header
    writer.writerow(['Participant ID', 'Date', 'Survey - Override',
                     'Phone Agent - Override', 'Wearable - Override'])

    participants = Participant.objects.all()

    print "Got participants"
    for participant in participants:
        # get their start/end dates

        overrides = ComplianceOverride.objects.filter(participant=participant).order_by('compliance_override_date')

        dates = {}

        for compliance in overrides:
            next_date = compliance.compliance_override_date.strftime('%Y-%m-%d')
            if next_date not in dates:
                dates[next_date] = [
                    None,
                    None,
                    None
                ]
            if compliance.compliance_type.type_name == 'Survey':
                dates[next_date][0] = compliance.compliance_override_value
            if compliance.compliance_type.type_name == 'Phone Agent':
                dates[next_date][1] = compliance.compliance_override_value
            if compliance.compliance_type.type_name == 'Wearable':
                dates[next_date][2] = compliance.compliance_override_value

            if next_date in dates:
                row = [participant.participant_id, next_date]
                row.extend(dates[next_date])

                writer.writerow(row)

    print "Total time: ", (datetime.datetime.now() - start)
    return response


@staff_member_required
def get_all_participant_daily(request):
    response = HttpResponse(FileWrapper(file('/tmp/AllParticipantsDaily.zip', 'rb')), content_type='application/zip')
    response['Content-Disposition'] = 'attachment; filename=AllParticipantsDaily.zip'
    return response


@staff_member_required
def get_all_issues(request):
    open_issues = Issue.objects.all().order_by('date_reported')

    start = datetime.datetime.now()
    # print "Starting now:", start

    issues = []

    for issue in open_issues:
        assigned_to = None
        if issue.assigned_to:
            assigned_to = issue.assigned_to.username

        new_issue_obj = {
            'issue_id': issue.id,
            'participant_id': issue.reported_by_participant.participant_id,
            'participant_username': issue.reported_by_user.username,
            'date_reported': issue.date_reported.strftime('%Y-%m-%d %H:%M:%S'),
            'status': issue.status.status,
            'subject': issue.subject,
            'description': issue.description,
            'category': issue.category.name,
            'assigned_to': assigned_to,
            'preferred_contact_method': issue.preferred_contact_method.contact_method,
            'messages': []
        }

        notes = issue.note_issue.all()
        for note in notes:
            new_note = {
                'author': note.author.username,
                'date': note.date.strftime('%Y-%m-%d %H:%M:%S'),
                'message': note.note
            }
            new_issue_obj['messages'].append(new_note)

        issues.append(new_issue_obj)

    end = datetime.datetime.now()
    # print "Completed: ", (end - start)
    return JsonResponse(issues, safe=False)

from django.db import models


class Chart(models.Model):
    name = models.CharField(max_length=200)

    def __str__(self):
        return self.name


class ChartSeries(models.Model):
    name = models.CharField(max_length=200)
    chart = models.ForeignKey('Chart', related_name='chart_series')

    def __str__(self):
        return self.chart.name + ': '+self.name


class ChartDatum(models.Model):
    chart_series = models.ForeignKey('ChartSeries', related_name='series_data')
    date = models.DateField(blank=True, null=True)
    value = models.DecimalField(max_digits=6, decimal_places=2)

    def __str__(self):
        return self.chart_series.chart.name + ': '+self.chart_series.name + ' ['+self.date.strftime('%Y-%m-%d')+': '+str(self.value)+']'

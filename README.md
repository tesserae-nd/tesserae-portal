# Tesserae Portal
### for the MOSAIC Project with IARPA

For more information see: [IARPA MOSAIC](https://www.iarpa.gov/index.php/research-programs/mosaic)

Notre Dame PI: [Aaron Striegel](mailto:striegel@nd.edu)

#### Deployment Notes

Installing Gentelella Theme with Bower

    cd tesserae/static/  
    bower install gentelella --save
    bower install highcharts --save
    
Disable PNotify

Open `static/bower_components/gentelella/build/js/custom.js`

Comment out lines 1843 -> 1863 (try searching for "new PNotify")   
** These are what cause the "PNotify" example to show up on all pages (annoying!!)

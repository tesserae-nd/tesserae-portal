from django.core.management.base import BaseCommand, CommandError
from crons.models import Chart, ChartSeries, ChartDatum
from django.conf import settings
import datetime
from tesserae.apps.compliance_app.compliance_calculations import last_30_days_average_compliance, \
    count_participants_last_30_days, count_overall_compliant_last_30_days, count_compliant_participants_last_30_days
from tesserae.apps.compliance_app.models import ParticipantCompliance


class Command(BaseCommand):
    help = 'Precomputes home page charts'

    def add_arguments(self, parser):
        parser.add_argument(
            '--all',
            dest='all',
            action='store_true',
            help='Ignores the `HOMEPAGE_CHART_CALCULATION_UPDATE` setting and updates ALL data, as far back as data '
                 'exists.  Note: this is recommended upon first run.'
        )

    def handle(self, *args, **options):
        # do we have a setting for days to follow?
        days_to_calculate = 30

        if options['all']:
            # figure out how far back we need to go...
            min_date = ParticipantCompliance.objects.earliest('compliance_date')
            days_to_calculate = (datetime.datetime.now().date() - min_date.compliance_date).days
            self.stdout.write(self.style.WARNING(
                '!!Performing update on ALL data!!'))
            self.stdout.write(self.style.WARNING(
                '\tUpdating data for the past '+str(days_to_calculate)+' days...'
            ))

        else:
            if settings.HOMEPAGE_CHART_CALCULATION_UPDATE:
                days_to_calculate = settings.HOMEPAGE_CHART_CALCULATION_UPDATE
            else:
                self.std.write("No setting defined for historic calcuation in days "
                               "(setting.HOMEPAGE_CHART_CALCULATION_UPDATE)...defaulting to "
                               ""+str(days_to_calculate)+" days.")
        # Run through all charts
        charts = Chart.objects.all()

        if len(charts) == 0:
            self.stdout.write(
                self.style.ERROR('Empty Dataset Error: You have no charts loaded in.  You can load the default charts '
                                 'and their stream definitions with: `python manage.py loaddata crons/fixtures`')
            )
            return 0

        self.stdout.write("Running calculations...")
        timing_start = datetime.datetime.now()

        for chart in charts:
            self.stdout.write("\tCalculating `"+chart.name+'`...')
            serieses = chart.chart_series.all()

            for series in serieses:
                # start 30 days ago
                current_date = (datetime.datetime.now() - datetime.timedelta(days=days_to_calculate)).date()
                date_iter = 0
                while current_date <= datetime.datetime.now().date():
                    # What function should we call if the chart is the "Average Compliance"?
                    if series.chart.name == 'Average Compliance':
                        # get the average value for just the current date - this one's eas since we pass in the
                        # series name to the function
                        calculated_value = last_30_days_average_compliance([current_date], series.name)[0]

                    # But if the chart is the "Overall Compliance" chart...
                    if series.chart.name == 'Overall Compliance':
                        # then we call different functions to see what the total count of participants on that day was,
                        # or how many were actually compliant
                        if series.name == 'Total Participants':
                            calculated_value = count_participants_last_30_days([current_date])[0]
                        if series.name == 'Compliant Participants':
                            calculated_value = count_overall_compliant_last_30_days([current_date])[0]

                    # And if the cahrt is the "Stream Compliance"
                    if series.chart.name == 'Stream Compliance':
                        # this one is also easy since we pass in the stream name into the function
                        calculated_value = count_compliant_participants_last_30_days([current_date], series.name)[0]

                    value, created = ChartDatum.objects.get_or_create(
                        chart_series=series,
                        date=datetime.datetime.fromtimestamp(calculated_value[0] / 1000),
                        defaults={
                            'value': calculated_value[1]
                        }
                    )
                    value.value = calculated_value[1]
                    value.save()

                    current_date = current_date + datetime.timedelta(days=1)
                    date_iter += 1

            self.stdout.write(self.style.SUCCESS('\tSuccessfully updated '
                                                 ''+str(days_to_calculate)+' days worth of `'+chart.name+'` data\n'))
        timing_end = datetime.datetime.now()

        self.stdout.write(self.style.SUCCESS(
            'Completed calculations in ['+str(timing_end - timing_start)+']'
        ))

from base import *

DEBUG = True

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'i(m1^$l4u191&)ky2q5e*8a@gv#zaok03jyymfs#%&#m-!ay+i'

ALLOWED_HOSTS = ['tesserae-portal.crc.nd.edu']

DATABASES = {
	'default': {
		'ENGINE': 'django.db.backends.postgresql_psycopg2',
		'NAME': 'tesserae_portal',
		'USER': 'portal_admin',
		'PASSWORD': 'There is no room for despair.',
		'PORT': '5432',
		'HOST': 'localhost'
	}
}

TIME_ZONE = 'EST'


import csv

from dateutil import parser
from datetime import datetime, timedelta

import json

from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.core.files.storage import default_storage
from django.http.response import HttpResponseRedirect, HttpResponse
from django.shortcuts import render
from django.urls.base import reverse_lazy
from django.utils.datastructures import MultiValueDictKeyError
from django.views.generic import ListView, TemplateView

from tesserae.apps.compliance_app.compliance_calculations import get_last_30_days, \
    compute_cumulative_compliance_over_date_range, get_date_list, \
    compute_cumulative_compliance_over_date_range_with_overrides
from tesserae.apps.compliance_app.models import ParticipantCompliance, ComplianceOverride, ComplianceType, \
    DetailedCompliance

from tesserae.apps.participant_app.models import Participant
import pika
from django.conf import settings
from django.db.models import Max


class MySurveyComplianceList(LoginRequiredMixin, UserPassesTestMixin, ListView):
    """
    List View to show a user's survey compliance details
    """
    model = ParticipantCompliance
    template_name = 'compliance/my_compliance_details.html'

    def get_context_data(self, **kwargs):
        context = super(MySurveyComplianceList, self).get_context_data(**kwargs)
        my_participant = Participant.objects.filter(gmail=self.request.user.email)
        compliances = ParticipantCompliance.objects.filter(participant=my_participant,
                                                           compliance_date__gte=(my_participant[0].
                                                                                 registration_date + timedelta(days=1)),
                                                           compliance_type__type_name=
                                                           "Survey").order_by('-compliance_date')
        compliance_list = []
        for compliance in compliances:
            value = compliance.compliance_value
            # are there any overrides?
            overrides = ComplianceOverride.objects.filter(participant=my_participant,
                                                          compliance_override_date=compliance.compliance_date,
                                                          compliance_type=compliance.compliance_type)
            if len(overrides) > 0:
                value = overrides[0].compliance_override_value

            compliance_list.append({
                'compliance_type': compliance.compliance_type,
                'compliance_date': compliance.compliance_date,
                'compliance_value': value
            })
        # context['my_compliance'] = ParticipantCompliance.objects.filter(participant=my_participant,
        #                                                                 compliance_date__gte=(my_participant[0].
        #                                                                 registration_date+timedelta(days=1)),
        #                                                                 compliance_type__type_name=
        #                                                                 "Wearable").order_by('-compliance_date')
        context['my_compliance'] = compliance_list
        context['compliance_type'] = "Survey"
        # print(context['my_compliance'])
        return context

    def test_func(self):
        # Testing that a user is not a staff member
        if self.request.user.is_staff:
            return False
        else:
            return True


class MyWearableComplianceList(LoginRequiredMixin, UserPassesTestMixin, ListView):
    """
    List View to show a user's wearable compliance details
    """
    model = ParticipantCompliance
    template_name = 'compliance/my_compliance_details.html'

    def get_context_data(self, **kwargs):
        context = super(MyWearableComplianceList, self).get_context_data(**kwargs)
        my_participant = Participant.objects.filter(gmail=self.request.user.email)

        compliances = ParticipantCompliance.objects.filter(participant=my_participant,
                                                                        compliance_date__gte=(my_participant[0].
                                                                        registration_date+timedelta(days=1)),
                                                                        compliance_type__type_name=
                                                                        "Wearable").order_by('-compliance_date')
        compliance_list = []
        for compliance in compliances:
            value = compliance.compliance_value
            # are there any overrides?
            overrides = ComplianceOverride.objects.filter(participant=my_participant,
                                                          compliance_override_date=compliance.compliance_date,
                                                          compliance_type=compliance.compliance_type)
            if len(overrides) > 0:
                value = overrides[0].compliance_override_value

            compliance_list.append({
                'compliance_type': compliance.compliance_type,
                'compliance_date': compliance.compliance_date,
                'compliance_value': value
            })
        # context['my_compliance'] = ParticipantCompliance.objects.filter(participant=my_participant,
        #                                                                 compliance_date__gte=(my_participant[0].
        #                                                                 registration_date+timedelta(days=1)),
        #                                                                 compliance_type__type_name=
        #                                                                 "Wearable").order_by('-compliance_date')
        context['my_compliance'] = compliance_list
        context['compliance_type'] = "Wearable"
        # print(context['my_compliance'])
        return context

    def test_func(self):
        # Testing that a user is not a staff member
        if self.request.user.is_staff:
            return False
        else:
            return True


class MyPhoneAgentComplianceList(LoginRequiredMixin, UserPassesTestMixin, ListView):
    """
    List View to show a user's phone agent compliance details
    """
    model = ParticipantCompliance
    template_name = 'compliance/my_compliance_details.html'

    def get_context_data(self, **kwargs):
        context = super(MyPhoneAgentComplianceList, self).get_context_data(**kwargs)
        my_participant = Participant.objects.filter(gmail=self.request.user.email)
        compliances = ParticipantCompliance.objects.filter(participant=my_participant,
                                                           compliance_date__gte=(my_participant[0].
                                                                                 registration_date + timedelta(days=1)),
                                                           compliance_type__type_name=
                                                           "Phone Agent").order_by('-compliance_date')
        compliance_list = []
        for compliance in compliances:
            value = compliance.compliance_value
            # are there any overrides?
            overrides = ComplianceOverride.objects.filter(participant=my_participant,
                                                          compliance_override_date=compliance.compliance_date,
                                                          compliance_type=compliance.compliance_type)
            if len(overrides) > 0:
                value = overrides[0].compliance_override_value

            compliance_list.append({
                'compliance_type': compliance.compliance_type,
                'compliance_date': compliance.compliance_date,
                'compliance_value': value
            })
        # context['my_compliance'] = ParticipantCompliance.objects.filter(participant=my_participant,
        #                                                                 compliance_date__gte=(my_participant[0].
        #                                                                 registration_date+timedelta(days=1)),
        #                                                                 compliance_type__type_name=
        #                                                                 "Wearable").order_by('-compliance_date')
        context['my_compliance'] = compliance_list
        context['compliance_type'] = "Phone Agent"
        # print(context['my_compliance'])
        return context

    def test_func(self):
        # Testing that a user is not a staff member
        if self.request.user.is_staff:
            return False
        else:
            return True


class ComplianceList(LoginRequiredMixin, UserPassesTestMixin, TemplateView):
    """
    Template view for the staff compliance table showing all participants and their compliance values
    """
    template_name = 'compliance/staff_compliance_table.html'

    def test_func(self):
        return self.request.user.is_staff

    def get_context_data(self, **kwargs):
        context = super(ComplianceList, self).get_context_data(**kwargs)
        context['cohorts'] = Participant.objects.values('cohort').distinct().order_by('cohort')

        return context


@login_required
def ajax_compliance_list(request):
    """
    Ajax call on page load or filter submission to populate the compliance table view for staff
    :param request: 
    :return: 
    """
    # print("Populating compliance table ----------------------------------------------------------")
    context_dictionary = {}

    # default date range is the last 30 days starting yesterday
    yesterday = datetime.today() - timedelta(1)
    sorted_date_list = get_last_30_days(yesterday)

    # Get compliance value range from filter
    start_val = request.GET['start_value']
    end_val = request.GET['end_value']
    toggle_overrides = request.GET['overrides']
    # print("Are we showing the overrides? ", toggle_overrides)

    try:
        start_date = request.GET['start_date']
        end_date = request.GET['end_date']

        sorted_date_list = get_date_list(start_date, end_date)

    except MultiValueDictKeyError:
        pass

    print "Filtering on:", sorted_date_list
    if request.GET['cohort'] != '':
        # All participants registered in the study
        participant_users = Participant.objects\
            .filter(registration_date__gte=datetime.strptime(request.GET['registration_start'], '%Y-%m-%d'),
                    registration_date__lte=datetime.strptime(request.GET['registration_end'], '%Y-%m-%d'),
                    cohort=request.GET['cohort'])
    else:
        # All participants registered in the study
        participant_users = Participant.objects \
            .filter(registration_date__gte=datetime.strptime(request.GET['registration_start'], '%Y-%m-%d'),
                    registration_date__lte=datetime.strptime(request.GET['registration_end'], '%Y-%m-%d'))

    all_participant_compliances = []

    # For all participant_app, get the compliance values
    for participant in participant_users:

        compliance_dict = {}

        # If toggle_overrides is false, just use raw compliance data
        if toggle_overrides == 'false':
            compliance_dict = {
                'months_survey_compliance':
                    compute_cumulative_compliance_over_date_range(sorted_date_list,
                                                                  participant, 'Survey')['score'],
                'months_wearable_compliance':
                    compute_cumulative_compliance_over_date_range(sorted_date_list,
                                                                  participant, 'Wearable')['score'],
                'months_phone_agent_compliance':
                    compute_cumulative_compliance_over_date_range(sorted_date_list,
                                                                  participant, 'Phone Agent')['score'],
                'months_overall_compliance':
                    compute_cumulative_compliance_over_date_range(sorted_date_list,
                                                                  participant, 'Overall')['score']
            }

        # If toggle overrides is true, check for overrides before just using raw compliance data
        elif toggle_overrides == 'true':

            average_survey_compliance = compute_cumulative_compliance_over_date_range_with_overrides(sorted_date_list,
                                                                                                     participant,
                                                                                                     'Survey')
            average_wearable_compliance = compute_cumulative_compliance_over_date_range_with_overrides(sorted_date_list,
                                                                                                       participant,
                                                                                                       'Wearable')
            average_phone_compliance = compute_cumulative_compliance_over_date_range_with_overrides(sorted_date_list,
                                                                                                    participant,
                                                                                                    'Phone Agent')
            average_overall_compliance = float(average_survey_compliance['score'] + average_wearable_compliance['score']
                                               + average_phone_compliance['score'])/3

            compliance_dict = {
                'months_survey_compliance': average_survey_compliance['score'],
                'months_wearable_compliance': average_wearable_compliance['score'],
                'months_phone_agent_compliance': average_phone_compliance['score'],
                'months_overall_compliance': average_overall_compliance
            }

        participant_compliance = {
            'participant': participant.participant_id,
            'backend_participant_id': participant.backend_participant_id,
            'compliances': compliance_dict
        }

        participant_overall_comp = participant_compliance['compliances']['months_overall_compliance']

        if int(start_val) <= participant_overall_comp <= int(end_val):
            all_participant_compliances.append(participant_compliance)

    context_dictionary['all_participant_compliances'] = all_participant_compliances

    return render(request, "compliance/compliance_table.html", context_dictionary)


class UploadOverrides(LoginRequiredMixin, UserPassesTestMixin, TemplateView):
    """
    Template view for the staff compliance table showing all participants and their compliance values
    """
    template_name = 'compliance/upload_overrides.html'

    def test_func(self):
        return self.request.user.is_staff


@login_required
def upload_override_file(request):
    """
    View to read and save the compliance override data from the CSV file.
    :param request:
    :return:
    """
    if request.method == 'POST' and request.FILES['override_file']:

        filename = request.FILES['override_file'].name  # received file name
        file_obj = request.FILES['override_file']

        with default_storage.open('override_files/' + filename, 'wb+') as destination:
            for chunk in file_obj.chunks():
                destination.write(chunk)

        with default_storage.open('override_files/' + filename) as csvfile:
            reader = csv.DictReader(csvfile)
            for row in reader:
                try:
                    participant = Participant.objects.get(participant_id=row['participant'])
                    date_obj = parser.parse(row['compliance_override_date'])
                    print(date_obj)
                    compliance_type_obj = ComplianceType.objects.get(type_name=row['compliance_type'])
                    override_obj, created = ComplianceOverride.objects.get_or_create(
                        participant=participant,
                        compliance_override_date=date_obj,
                        compliance_type=compliance_type_obj,
                        defaults={'compliance_override_value': int(float(row['compliance_override_value']))}
                    )
                    override_obj.compliance_override_value = int(float(row['compliance_override_value']))
                    override_obj.save()
                except Exception as err:
                    print(type(err), err)

                    # If unsuccessful, go to the success page
                    #failure_url = reverse_lazy('portal:compliance:failure_upload_overrides_file')

                    return render(request, 'compliance/failed_upload_overrides.html', override_error_lookup(err, row, reader.fieldnames))

    # If successful, go to the success page
    success_url = reverse_lazy('portal:compliance:success_upload_overrides_file')
    return HttpResponseRedirect(success_url)


def override_error_lookup(error, row, fields):
    print(type(error))
    if type(error) is KeyError:
        try:
            return {'type': 'CSV header missing a value',
                    'specific': ' Could not find the header name: `'+error[0]+'` in the provided fields: '+json.dumps(fields)}
        except UnicodeDecodeError:
            bad_field = 1
            for field in fields:
                try:
                    json.dumps(field)
                    bad_field += 1
                except UnicodeDecodeError as e:
                    # print(dir(e))
                    return {'type': 'Bad character in one of the CSV headers',
                            'specific': ' the header in column '+str(bad_field)+' contains an invalid character -- '+repr(e)}
    if type(error) is Participant.DoesNotExist:
        return {'type': 'Participant does not exist',
                'specific': ' Could not find the participant in the following row:\n' + json.dumps(row)}

    return {'type': 'Unknown Error', 'specific': 'Please contact the site administrator.'}

class UploadComplianceOverridesSuccess(LoginRequiredMixin, UserPassesTestMixin, TemplateView):
    """
    Template view for a successful payment upload

    """
    template_name = 'compliance/success_upload_overrides.html'

    def get_context_data(self, **kwargs):
        context = super(UploadComplianceOverridesSuccess, self).get_context_data(**kwargs)

        return context

    def test_func(self):
        return self.request.user.is_staff


class UploadComplianceOverridesFailure(LoginRequiredMixin, UserPassesTestMixin, TemplateView):
    """
    Template view for a failed payment upload

    """
    template_name = 'compliance/failed_upload_overrides.html'

    def get_context_data(self, **kwargs):
        context = super(UploadComplianceOverridesFailure, self).get_context_data(**kwargs)

        return context

    def test_func(self):
        return self.request.user.is_staff


@login_required
def get_participant_compliance_profile(request, participant_id):
    if not request.user.is_staff:
        return HttpResponseRedirect('/')

    participant = Participant.objects.get(participant_id=participant_id)
    last_updated = ParticipantCompliance.objects.filter(participant_id=participant_id).aggregate(Max('calculated_on'))
    return render(request, 'compliance/participant_details.html', {'participant': participant,
                                                                   'last_updated': last_updated['calculated_on__max']})


@login_required
def get_all_detailed_compliance_page(request):
    if not request.user.is_staff:
        return HttpResponseRedirect('/')

    return render(request, 'compliance/all_compliance_table_page.html', {'cohorts': Participant.objects.values('cohort').distinct().order_by('cohort')})


@login_required
def get_participant_detailed_compliance_page(request, participant_id, major_stream):
    if not request.user.is_staff:
        return HttpResponseRedirect('/')

    participant = Participant.objects.get(participant_id=participant_id)
    major_stream = ComplianceType.objects.filter(type_name=major_stream)
    if len(major_stream) >= 1:
        major_stream = major_stream[0]
    else:
        major_stream = None

    return render(request, 'compliance/participant_daily_details.html', {'participant': participant,
                                                                         'major_stream': major_stream})


@login_required
def get_all_detailed_table(request):
    start_date = datetime.strptime(request.GET['start_date'], '%Y-%m-%d').date()
    end_date = datetime.strptime(request.GET['end_date'], '%Y-%m-%d').date()

    registration_start = datetime.strptime(request.GET['registration_start'], '%Y-%m-%d').date()
    registration_end = datetime.strptime(request.GET['registration_end'], '%Y-%m-%d').date()

    # get a list of participants who fall within the registration dates
    if request.GET['cohort'] != '':
        participants = Participant.objects.filter(registration_date__gte=registration_start,
                                                  registration_date__lte=registration_end,
                                                  cohort=request.GET['cohort']).order_by('id')
    else:
        participants = Participant.objects.filter(registration_date__gte=registration_start,
                                                  registration_date__lte=registration_end).order_by('id')

    ########### MAJOR STREAMS #################
    major_streams = [
        {'major_name': 'Overall', 'minor_name': ''},
        {'major_name': 'Survey', 'minor_name': ''},
        {'major_name': 'Wearable', 'minor_name': ''}
    ]

    ########### MINOR STREAMS #################
    # now get a list of distinct stream minors
    minor_streams = DetailedCompliance.objects.filter(compliance_type__type_name='Phone Agent').values(
        "detailed_type").distinct().order_by('compliance_type__type_name')
    minor_stream_list = []
    for minor_stream in minor_streams:
        minor_stream_list.append({
            'minor_name': minor_stream['detailed_type'],
            'major_name': DetailedCompliance.objects.filter(detailed_type=minor_stream['detailed_type'])[
                0].compliance_type.type_name
        })

    participant_list = []
    for participant in participants:
        p_start = start_date
        if p_start <= participant.registration_date:
            p_start = participant.registration_date + timedelta(days=1)
        participant_obj = {
            'participant_id': participant.participant_id,
            'values': []
        }
        participant_obj['values'].append({
            'value': compute_cumulative_compliance_over_date_range([p_start, end_date], participant, 'Overall')['score'],
            'major': None
        })
        participant_obj['values'].append({
            'value': compute_cumulative_compliance_over_date_range([p_start, end_date], participant, 'Survey')['score'],
            'major': 'Survey'
        })
        participant_obj['values'].append({
            'value': compute_cumulative_compliance_over_date_range([p_start, end_date], participant, 'Wearable')['score'],
            'major': 'Wearable'
        })

        # for each participant, run through the minor stream types
        for stream in minor_stream_list:
            # get the compliance values for each participant/stream
            compliances = DetailedCompliance.objects.filter(participant=participant, date__gte=p_start,
                                                            date__lte=end_date, detailed_type=stream['minor_name'])
            if len(compliances) == 0:
                avg = 0.0
            else:
                sum = 0.0
                for c in compliances:
                    sum += float(c.score)
                avg = (float(sum) / float(len(compliances)) * 100)
            participant_obj['values'].append({
                'value': avg,
                'major': stream['major_name']
            })
        participant_list.append(participant_obj)

    category_list = major_streams + minor_stream_list

    return render(request, 'compliance/all_compliance_details_table.html', {'categories': category_list,
                                                                            'participants': participant_list})

@login_required
def get_participant_detailed_table(request):
    # get a list of participants who fall within the registration dates
    participant = Participant.objects.get(participant_id=request.GET['participant_id'])


    start_date = datetime.strptime(request.GET['start_date'], '%Y-%m-%d').date()
    if start_date <= participant.registration_date:
        start_date = participant.registration_date + timedelta(days=1)
    end_date = datetime.strptime(request.GET['end_date'], '%Y-%m-%d').date()

    # now get a list of distinct stream minors
    minor_streams = DetailedCompliance.objects.filter(compliance_type__id=request.GET['compliance_id'])\
        .values("detailed_type").distinct()

    minor_stream_list = []
    for minor_stream in minor_streams:
        minor_stream_list.append({
            'minor_name': minor_stream['detailed_type'],
            'major_name': DetailedCompliance.objects.filter(detailed_type=minor_stream['detailed_type'])
            [0].compliance_type.type_name
        })

    compliance_data = []
    current_date = start_date
    while current_date <= end_date:
        compliance_date = {
            'date': current_date.strftime('%m/%d/%Y'),
            'values': []
        }

        # for each day, get the minor stream type's compliances
        for minor_stream in minor_stream_list:
            compliance_score = DetailedCompliance.objects.filter(detailed_type=minor_stream['minor_name'],
                                                                 date=current_date,
                                                                 participant=participant)
            if len(compliance_score) >= 1:
                score = (compliance_score[0].score * 100)
            else:
                score = 0.0

            compliance_date['values'].append({
                'score': score,
                'major': minor_stream['major_name'],
                'date_for_link': current_date.strftime('%Y-%m-%d')
            })

        compliance_data.append(compliance_date)

        current_date += timedelta(days=1)


    return render(request, 'compliance/participant_daily_details_table.html', {'categories': minor_stream_list,
                                                                            'compliance_data': compliance_data,
                                                                               'participant': participant})



@login_required
def get_participant_compliance_table(request, participant_id):
    if not request.user.is_staff:
        return HttpResponseRedirect('/')

    participant = Participant.objects.get(participant_id=participant_id)

    compliance_values = []
    start_date = datetime.strptime(request.GET['start_date'], '%Y-%m-%d').date()
    end_date = datetime.strptime(request.GET['end_date'], '%Y-%m-%d').date()

    if start_date <= participant.registration_date:
        start_date = participant.registration_date + timedelta(days=1)

    current_date = start_date
    while current_date <= end_date:
        sorted_date_list = [current_date]
        compliance = {
            'date': current_date.strftime('%m/%d/%Y'),
            'date_for_link': current_date.strftime('%Y-%m-%d'),
            'days_survey_compliance':
                compute_cumulative_compliance_over_date_range(sorted_date_list,
                                                              participant, 'Survey'),
            'days_wearable_compliance':
                compute_cumulative_compliance_over_date_range(sorted_date_list,
                                                              participant, 'Wearable'),
            'days_phone_agent_compliance':
                compute_cumulative_compliance_over_date_range(sorted_date_list,
                                                              participant, 'Phone Agent'),
            'days_overall_compliance':
                compute_cumulative_compliance_over_date_range(sorted_date_list,
                                                              participant, 'Overall')
        }

        compliance_values.append(compliance)
        current_date += timedelta(days=1)

    print(compliance_values)

    return render(request, 'compliance/participant_compliance_table.html', {'participant': participant,
                                                                            'compliance_values': compliance_values})


@login_required
def get_participant_compliance_details(request, participant_id, compliance_type, date):
    if not request.user.is_staff:
        return HttpResponseRedirect('/')

    # get all records of participant compliance details for this date, type, participant
    details = DetailedCompliance.objects.filter(participant_id=participant_id,
                                                compliance_type__type_name=compliance_type,
                                                date=date)

    # master list we return
    detail_list = []

    # setup categories (these will be the headers across the top of the table
    distinct_types = []
    categories = []
    for d in details:
        if d.detailed_type not in distinct_types:
            categories.append({
                'name': d.detailed_type,
                'score': d.score * 100
            })
            distinct_types.append(d.detailed_type)

    # now for each half hour for today...
    start = datetime.strptime(date, '%Y-%m-%d')
    end = start + timedelta(days=1)
    current = start
    count = 0
    while current < end:
        # create a new object
        time_data = {
            'datetime': current.strftime('%m/%d/%Y %I:%M %p'),
            'time': current.strftime('%I:%M %p'),
            'values': []
        }

        # now loop through the details, looking for the "count" value in the data
        for detail in details:
            if detail.data:
                data_array = json.loads(detail.data)
                time_data['values'].append(data_array[count])
            else:
                time_data['values'].append(detail.score)

        detail_list.append(time_data)
        # increment our iterators
        count += 1
        current += timedelta(minutes=30)

    # print detail_list
    return render(request, 'compliance/detailed_compliance.html', {'details': detail_list,
                                                                   'date': date,
                                                                   'categories': categories,
                                                                   'compliance_type': compliance_type,
                                                                   'participant': Participant.objects.get(
                                                                       participant_id=participant_id)})


@login_required
def request_compliance_refresh(request):
    if 'portal_id' in request.GET and request.user.is_staff:
        participant = Participant.objects.get(participant_id=request.GET['portal_id'])
    else:
        # get this participant
        participant = Participant.objects.get(gmail=request.user.email)

    if participant is None:
        return HttpResponse(json.dumps({'error': 'No participant ID found.'}))

    credentials = pika.PlainCredentials(settings.RABBIT_USER, settings.RABBIT_PASSWORD)

    connection = pika.BlockingConnection(
        parameters=pika.ConnectionParameters(host=settings.RABBIT_HOST, credentials=credentials))

    channel = connection.channel()

    channel.queue_declare(queue='compliance_request')
    channel.basic_publish(exchange='', routing_key='compliance_request', body=json.dumps({
        'type': request.GET['type'],
        'portal_id': participant.participant_id
    }))

    return HttpResponse(json.dumps({'success': True}))
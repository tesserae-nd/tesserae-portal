from django.contrib import admin

# Register your models here.
from tesserae.apps.payment_app.models import Payment


@admin.register(Payment)
class PaymentAdmin(admin.ModelAdmin):
    list_display = ('participant', 'payment_date', 'payment_amount', 'payment_description')

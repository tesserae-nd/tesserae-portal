from django.apps import AppConfig


class BasePortalConfig(AppConfig):
    name = 'tesserae.apps.base_portal'

from django.apps import AppConfig


class PaymentAppConfig(AppConfig):
    name = 'tesserae.apps.payment_app'
